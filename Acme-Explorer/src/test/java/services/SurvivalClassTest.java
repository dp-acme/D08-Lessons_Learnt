package services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Coordinates;
import domain.Explorer;
import domain.Status;
import domain.SurvivalClass;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional
public class SurvivalClassTest extends AbstractTest {

	@Autowired
	private SurvivalClassService survivalClassService;

	@Autowired
	private TripService tripService;
	
	@Autowired
	private ExplorerService explorerService;
	
	@Autowired
	private ApplyForService applyForService;

	@Test
	public void testSaveSurvival() {
		SurvivalClass survivalClass, saved;
		List<Trip> trips;
		Coordinates location;

		location = new Coordinates();
		trips = (List<Trip>) tripService.findAll();
		this.authenticate(trips.get(2).getManager().getUserAccount()
				.getUsername());
		survivalClass = survivalClassService.create(trips.get(2));
		survivalClass.setDescription("Descripción prueba");
		location.setLatitude(3.0);
		location.setLongitude(5.0);
		location.setName("location");
		survivalClass.setLocation(location);
		survivalClass.setMoment(trips.get(2).getPublicationDate());
		survivalClass.setTitle("Titulo prueba");

		saved = survivalClassService.save(survivalClass);
		Assert.isTrue(survivalClassService.findAll().contains(saved));
		Assert.isTrue(survivalClass.getDescription().equals(
				saved.getDescription()));
		// Assert.isTrue(survivalClass.getLocation().equals(saved.getLocation()));
		Assert.isTrue(survivalClass.getLocation().getLatitude() == saved
				.getLocation().getLatitude());
		Assert.isTrue(survivalClass.getLocation().getLongitude() == saved
				.getLocation().getLongitude());
		Assert.isTrue(survivalClass.getLocation().getName() == saved
				.getLocation().getName());
		Assert.isTrue(survivalClass.getTrip().getManager().equals(saved.getTrip().getManager()));
		Assert.isTrue(survivalClass.getMoment().equals(saved.getMoment()));
		Assert.isTrue(survivalClass.getTitle().equals(saved.getTitle()));
		Assert.isTrue(survivalClass.getTrip().equals(saved.getTrip()));
		this.unauthenticate();

	}
	@Test
	public void testDeleteSurvival() {
		List<SurvivalClass> survivals;
		SurvivalClass survival;

		survivals = (List<SurvivalClass>) survivalClassService.findAll();
		survival = survivals.get(0);
		
		this.authenticate(survival.getTrip().getManager().getUserAccount().getUsername());
		survivalClassService.delete(survival);
		Assert.isNull(survivalClassService.findOne(survival.getId()));
		this.unauthenticate();

	}
	@Test
	public void testFindAll() {
		Collection<SurvivalClass> result;

		result = survivalClassService.findAll();

		Assert.notNull(result);

		this.unauthenticate();

	}
	@Test
	public void testFindOne(){
		List<SurvivalClass> all;
		SurvivalClass survivalClassFromList, survivalClassFromRepository;
		
		all = (List<SurvivalClass>) survivalClassService.findAll();
		Assert.notEmpty(all);
		survivalClassFromList = all.get(0);
		
		survivalClassFromRepository = survivalClassService.findOne(survivalClassFromList.getId());
		
		Assert.notNull(survivalClassFromRepository);
		Assert.isTrue(survivalClassFromList.equals(survivalClassFromRepository));
		
		this.unauthenticate();
	}
	
	@Test
	public void testJoinExplorer(){
		List<Explorer> explorers, explo;
		Explorer explorer;
		List<SurvivalClass> survivals;
		SurvivalClass survival;

		
		
		explorers = (List<Explorer>) explorerService.findAll();
		explorer = explorers.get(1);
		survivals = (List<SurvivalClass>) survivalClassService.findAll();
		survival = null;
		for(SurvivalClass s: survivals){
			if(applyForService.getApplicationFromExplorerAndTrip(explorer, s.getTrip())!=null&&
					applyForService.getApplicationFromExplorerAndTrip(explorer, s.getTrip()).getStatus().equals(Status.ACCEPTED)){
				survival = s;
				break;
			}
		}
		if(survival!=null){
			this.authenticate(explorer.getUserAccount().getUsername());
			
			survivalClassService.joinSurvivalClass(explorer, survival);
			explo = (List<Explorer>) survival.getExplorers();
			Assert.isTrue(explo.contains(explorer));
			this.unauthenticate();
		}
	}

}

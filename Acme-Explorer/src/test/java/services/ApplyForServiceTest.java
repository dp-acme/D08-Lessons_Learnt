package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.ApplyFor;
import domain.CreditCard;
import domain.Explorer;
import domain.Status;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml"})
@Transactional


public class ApplyForServiceTest extends AbstractTest {

	@Autowired
	private ApplyForService applyForService;
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private ExplorerService explorerService;
	
	@Test
	public void saveApplyForTest(){
		this.authenticate("explorer1");
		ApplyFor applyFor, saved, saved1;
		CreditCard creditCard;
		List<Trip> trips;
		Trip trip;
		
		trips = (List<Trip>) tripService.findAll();
		trip = trips.get(2);
		applyFor = applyForService.create(trip);
		applyFor.setComments("Comments");
		creditCard = new CreditCard();
		creditCard.setBrandName("Visa");
		creditCard.setCvv(222);
		creditCard.setExpirationMonth(11);
		creditCard.setExpirationYear(2021);
		creditCard.setHolderName("explorer1");
		creditCard.setNumber("4822183306245293");
		applyFor.setCreditCard(creditCard);
		
		saved = applyForService.save(applyFor);
		Assert.isTrue(applyForService.findAll().contains(saved));
		Assert.isTrue(saved.getComments().equals(applyFor.getComments()));
		Assert.isTrue(saved.getCreditCard().getBrandName().equals(applyFor.getCreditCard().getBrandName()));
		Assert.isTrue(saved.getCreditCard().getCvv().equals(applyFor.getCreditCard().getCvv()));
		Assert.isTrue(saved.getCreditCard().getExpirationMonth().equals(applyFor.getCreditCard().getExpirationMonth()));
		Assert.isTrue(saved.getCreditCard().getExpirationYear().equals(applyFor.getCreditCard().getExpirationYear()));
		Assert.isTrue(saved.getCreditCard().getHolderName().equals(applyFor.getCreditCard().getHolderName()));
		Assert.isTrue(saved.getCreditCard().getNumber().equals(applyFor.getCreditCard().getNumber()));
		Assert.isTrue(saved.getExplorer().equals(applyFor.getExplorer()));
		Assert.isTrue(saved.getMoment().equals(applyFor.getMoment()));
		Assert.isTrue(saved.getStatus().getStatus().equals(applyFor.getStatus().getStatus()));
		Assert.isTrue(saved.getTrip().equals(applyFor.getTrip()));
		
		saved.setComments("Comments cambiados");
		saved1 = applyForService.save(saved);
		Assert.isTrue(saved.getComments().equals(saved1.getComments()));
		this.unauthenticate();
	}
	
	@Test
	public void testFindAll(){
		Collection<ApplyFor> result;
		
		result = applyForService.findAll();
		
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testFindOne(){
		List<ApplyFor> all;
		ApplyFor applyForFromList, applyForFromRepository;
		
		all = (List<ApplyFor>) applyForService.findAll();
		Assert.notEmpty(all);
		applyForFromList = all.get(0);
		
		applyForFromRepository = applyForService.findOne(applyForFromList.getId());
		
		Assert.notNull(applyForFromRepository);
		Assert.isTrue(applyForFromList.equals(applyForFromRepository));
		
		this.unauthenticate();
	}
	
	@Test
	public void testChangeStatus(){
		this.authenticate("explorer1");
		ApplyFor applyFor, saved, saved1;
		List<Trip> trips;
		Trip trip;
		Status status;
		
		trips = (List<Trip>) tripService.findAll();
		trip = trips.get(2);
		applyFor = applyForService.create(trip);
		applyFor.setComments("Comments");
		
		saved = applyForService.save(applyFor);
		this.unauthenticate();
		this.authenticate(trip.getManager().getUserAccount().getUsername());
		
		status = new Status();
		status.setStatus(Status.DUE);
		saved.setStatus(status);
		
		
		saved1 = applyForService.changeStatus(saved);
		Assert.isTrue(saved.getStatus().getStatus().equals(saved1.getStatus().getStatus()));
	
	}
	
	@Test
	public void testByStatus(){
		List<Explorer> explorers;
		Explorer explorer;
		Map<Status, Collection<ApplyFor>> result; 
		
		result = new HashMap<Status, Collection<ApplyFor>>();
		explorers = (List<Explorer>) explorerService.findAll();
 		explorer = explorers.get(0);
		result = applyForService.getApplicationsGroupedByStatus(explorer);
		Assert.isTrue(!result.values().isEmpty());
		
	}
	@Test
	public void testByExplorerStatus(){
		List<Explorer> explorers;
		Explorer explorer;
		Status status;
		Collection<ApplyFor> result;
		
		status = new Status();
		status.setStatus(Status.ACCEPTED);
		explorers = (List<Explorer>) explorerService.findAll();
 		explorer = explorers.get(0);
		result = applyForService.getApplicationsFromExplorerAndStatus(explorer, status);
		Assert.isTrue(!result.isEmpty());
	}
	
	@Test
	public void testByExplorerTrip(){
		List<Explorer> explorers;
		Explorer explorer;
		List<Trip> trips;
		Trip trip;
		ApplyFor result;
		List<ApplyFor> applications;
		
		trips = (List<Trip>) tripService.findAll();
		trip = trips.get(0);
		explorers = (List<Explorer>) explorerService.findAll();
 		explorer = explorers.get(0);
		applications = (List<ApplyFor>) explorer.getApplications();
 		result = applyForService.getApplicationFromExplorerAndTrip(explorer, trip);
		Assert.isTrue(applications.get(0).equals(result));
	}
	
	@Test
	public void testFilterStatus(){
		Status status;
		Collection<ApplyFor> result;
		List<ApplyFor> applications;
		ApplyFor application;
		
		applications = new ArrayList<ApplyFor>(applyForService.findAll());
		application = applications.get(0);
		status = application.getStatus();
		
		result = applyForService.filterApplicationsByStatus(status);
		
		Assert.isTrue(result.contains(application));
	}	
}

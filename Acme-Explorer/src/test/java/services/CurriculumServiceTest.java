package services;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import utilities.AbstractTest;

import domain.Curriculum;
import domain.Ranger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class CurriculumServiceTest extends AbstractTest{

	@Autowired
	public CurriculumService curriculumService;
	
	@Autowired
	public ActorService actorService;
	
	@Autowired
	public EndorserService endorserService;
	
	@Autowired
	public EducationService educationService;
	
	@Autowired
	public MiscellaneousService miscellaneousService;
	
	@Autowired
	public ProfessionalService professionalService;
	
	@Test
	public void saveNewCurriculumTest(){
		Curriculum result;
		
		this.authenticate("ranger1");
		
		result = curriculumService.create();
		
		result.setName("TestName");
		result.setEmail("test@test.es");
		result.setPhone("123123123");
		result.setPhoto("http://www.photo.es");
		result.setPhoto("http://www.profile.es");

		result = curriculumService.save(result);
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void saveNewCurriculumTestBadAuth(){
		Curriculum result;
		
		this.authenticate("manager1");
		
		result = curriculumService.create();
		
		result.setName("TestName");
		result.setEmail("test@test.es");
		result.setPhone("123123123");
		result.setPhoto("http://www.photo.es");
		result.setPhoto("http://www.profile.es");

		result = curriculumService.save(result);
	}
	
	@Test
	public void editCurriculumTest(){
		Curriculum result;
		Ranger ranger; 
		
		this.authenticate("ranger1");
		
		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		result = null;
		
		for(Curriculum c : curriculumService.findAll()){
			if(c.getRanger().equals(ranger)){
				result = c;
				break;
			}
		}
				
		result = curriculumService.save(result);
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void editCurriculumTestBadAuth(){
		Curriculum result;
		Ranger ranger; 
		
		this.authenticate("ranger1");
		
		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		result = null;
		
		for(Curriculum c : curriculumService.findAll()){
			if(!c.getRanger().equals(ranger)){
				result = c;
				break;
			}
		}
				
		result = curriculumService.save(result);
	}
	
	@Test
	public void deleteCurriculumTest(){
		Curriculum result;
		Ranger ranger; 
		
		this.authenticate("ranger1");
		
		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		result = null;
		
		for(Curriculum c : curriculumService.findAll()){
			if(c.getRanger().equals(ranger)){
				result = c;
				break;
			}
		}
		
		curriculumService.delete(result);
				
		Assert.isTrue(!curriculumService.findAll().contains(result));
		Assert.isTrue(!endorserService.findAll().contains(result.getEndorserRecords()));
		Assert.isTrue(!educationService.findAll().contains(result.getEducationRecords()));
		Assert.isTrue(!miscellaneousService.findAll().contains(result.getMiscellaneousRecords()));
		Assert.isTrue(!professionalService.findAll().contains(result.getProfessionalRecords()));

	}
	
	@Test(expected = IllegalArgumentException.class)
	public void deleteCurriculumTestBadAuth(){
		Curriculum result;
		Ranger ranger; 
		
		this.authenticate("ranger1");
		
		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		result = null;
		
		for(Curriculum c : curriculumService.findAll()){
			if(!c.getRanger().equals(ranger)){
				result = c;
				break;
			}
		}
		
		curriculumService.delete(result);
				
		Assert.isTrue(!curriculumService.findAll().contains(result));
		Assert.isTrue(!endorserService.findAll().contains(result.getEndorserRecords()));
		Assert.isTrue(!educationService.findAll().contains(result.getEducationRecords()));
		Assert.isTrue(!miscellaneousService.findAll().contains(result.getMiscellaneousRecords()));
		Assert.isTrue(!professionalService.findAll().contains(result.getProfessionalRecords()));

	}
	
	
	
	@Test
	public void getCurriculumTickersTest(){
		List<String> tickers; 
		
		tickers = (List<String>) curriculumService.getCurriculumTickers();
		
		for(Curriculum c : curriculumService.findAll()){
			Assert.isTrue(tickers.contains(c.getTicker()));
		}
		
	}
	
	
	
}

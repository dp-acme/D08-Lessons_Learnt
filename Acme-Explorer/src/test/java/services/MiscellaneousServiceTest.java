package services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Miscellaneous;
import domain.Ranger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional
public class MiscellaneousServiceTest extends AbstractTest{
	
	@Autowired
	private MiscellaneousService miscellaneousService;
	
	@Autowired
	private RangerService rangerService;
	
	@Test
	public void testSaveNewMiscellaneous(){
		Miscellaneous miscellaneous, result;
		Ranger ranger;
		ranger=null;
		List<Miscellaneous> miscellaneousList;
		
		for(Ranger r : rangerService.findAll()){
			if(r.getCurriculum()!=null){
				ranger = r;
			}
		}
		super.authenticate(ranger.getUserAccount().getUsername());
		
		miscellaneous = miscellaneousService.create(ranger.getCurriculum());
		miscellaneous.setTitle("TestTitleMisc1");
		miscellaneous.setComments("TestCommentsMisc1");
		result = miscellaneousService.save(miscellaneous);

		miscellaneousList = new ArrayList<Miscellaneous>(miscellaneousService.findAll());
		Assert.isTrue(miscellaneousList.contains(result));
		super.authenticate(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSaveNewMiscellaneousBadAuth(){
		Miscellaneous miscellaneous, result;
		Ranger ranger;
		ranger=null;
		List<Miscellaneous> miscellaneousList;
		
		for(Ranger r : rangerService.findAll()){
			if(r.getCurriculum()!=null){
				ranger = r;
			}
		}
		super.authenticate("manager1");
		
		miscellaneous = miscellaneousService.create(ranger.getCurriculum());
		miscellaneous.setTitle("TestTitleMisc1");
		miscellaneous.setComments("TestCommentsMisc1");
		result = miscellaneousService.save(miscellaneous);

		miscellaneousList = new ArrayList<Miscellaneous>(miscellaneousService.findAll());
		Assert.isTrue(miscellaneousList.contains(result));
		super.authenticate(null);
	}

	@Test
	public void testSaveExistingMiscellaneous(){
		Miscellaneous miscellaneous, result;
		Ranger ranger;
		List<Miscellaneous> miscellaneousList;
		
		ranger=null;

		for(Ranger r : rangerService.findAll()){
			if(r.getCurriculum()!=null&&r.getCurriculum().getMiscellaneousRecords().size()!=0){
				ranger = r;
			}
		}
		super.authenticate(ranger.getUserAccount().getUsername());
		
		miscellaneousList = new ArrayList<Miscellaneous>(ranger.getCurriculum().getMiscellaneousRecords());
		miscellaneous = miscellaneousList.get(0);
		miscellaneous.setTitle("testAlternativeTitle");
		miscellaneousService.save(miscellaneous);
		result = miscellaneousService.findOne(miscellaneous.getId());
		
		Assert.isTrue(result.getTitle().equals("testAlternativeTitle"));
		super.authenticate(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSaveExistingMiscellaneousBadAuth(){
		Miscellaneous miscellaneous, result;
		Ranger ranger;
		List<Miscellaneous> miscellaneousList;
		
		ranger=null;

		for(Ranger r : rangerService.findAll()){
			if(r.getCurriculum()!=null&&r.getCurriculum().getMiscellaneousRecords().size()!=0){
				ranger = r;
			}
		}
		super.authenticate("manager1");
		
		miscellaneousList = new ArrayList<Miscellaneous>(ranger.getCurriculum().getMiscellaneousRecords());
		miscellaneous = miscellaneousList.get(0);
		miscellaneous.setTitle("testAlternativeTitle");
		miscellaneousService.save(miscellaneous);
		result = miscellaneousService.findOne(miscellaneous.getId());
		
		Assert.isTrue(result.getTitle().equals("testAlternativeTitle"));
		super.authenticate(null);
	}

	
	 @Test
	 public void testFindAllMiscellaneous(){
		List<Miscellaneous> miscellaneousList;
		miscellaneousList = new ArrayList<Miscellaneous>(miscellaneousService.findAll());
		Assert.notNull(miscellaneousList);
	 }
	 @Test
	 public void testFindOneMiscellaneous(){
		Miscellaneous miscellaneous;
		List<Miscellaneous> miscellaneousList;
		miscellaneousList = new ArrayList<Miscellaneous>(miscellaneousService.findAll());
		miscellaneous = miscellaneousService.findOne(miscellaneousList.get(0).getId());
		Assert.isTrue(miscellaneousList.contains(miscellaneous));
	 }
	 
	 @Test
	 public void testDeleteMiscellaneous(){
		Miscellaneous miscellaneous;
		Ranger ranger;
		List<Miscellaneous> miscellaneousList;
			
		 
		 ranger=null;
		 for(Ranger r : rangerService.findAll()){
				if(r.getCurriculum()!=null&&r.getCurriculum().getMiscellaneousRecords().size()!=0){
					ranger = r;
				}
			}
			super.authenticate(ranger.getUserAccount().getUsername());
			miscellaneousList =new ArrayList<Miscellaneous>(ranger.getCurriculum().getMiscellaneousRecords());
			miscellaneous = miscellaneousList.get(0);
			miscellaneousService.delete(miscellaneous);
			
			Assert.isTrue(!miscellaneousService.findAll().contains(miscellaneous));
	 }
	 
	@Test(expected = IllegalArgumentException.class)
	 public void testDeleteMiscellaneousBadAuth(){
			Miscellaneous miscellaneous;
			Ranger ranger;
			List<Miscellaneous> miscellaneousList;
			 
			 ranger=null;
			 for(Ranger r : rangerService.findAll()){
					if(r.getCurriculum()!=null&&r.getCurriculum().getMiscellaneousRecords().size()!=0){
						ranger = r;
					}
				}
				super.authenticate("manager1");
				miscellaneousList =new ArrayList<Miscellaneous>(ranger.getCurriculum().getMiscellaneousRecords());
				miscellaneous = miscellaneousList.get(0);
				miscellaneousService.delete(miscellaneous);
				
				Assert.isTrue(!miscellaneousService.findAll().contains(miscellaneous));
		 }
}

package services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import security.UserAccount;
import utilities.AbstractTest;
import domain.EmergencyContact;
import domain.Explorer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class EmergencyContactServiceTest extends AbstractTest{
	
	@Autowired 
	private EmergencyContactService emergencyContactService;
	
	@Autowired 
	private ActorService actorService;
	
	//***CRUD TEST*****
	
	@Test
	public void testFindAll(){
		Collection<EmergencyContact> result;
		
		result = emergencyContactService.findAll();
		
		Assert.notNull(result);
	}
	
	@Test
	public void testFindOne(){
		List<EmergencyContact> all;
		EmergencyContact emergencyContactFromList, emergencyContactFromRepository;
		
		all = (List<EmergencyContact>) emergencyContactService.findAll();
		Assert.notEmpty(all);
		emergencyContactFromList = all.get(0);
		
		emergencyContactFromRepository = emergencyContactService.findOne(emergencyContactFromList.getId());
		
		Assert.notNull(emergencyContactFromRepository);
		Assert.isTrue(emergencyContactFromList.equals(emergencyContactFromRepository));
	}
	
	@Test
	public void testSaveAuth(){
		this.authenticate("Explorer1"); //Se sobreentiende que es un Explorer para evitar alargar el m�todo innecesariamente
		
		UserAccount principal;
		Explorer explorer;
		EmergencyContact emergencyContact;
		
		principal = LoginService.getPrincipal();
		explorer = (Explorer) actorService.findByUserAccountId(principal.getId());
		emergencyContact = emergencyContactService.create();
		emergencyContact.setEmail("email@email.com");
		emergencyContact.setName("name");
		emergencyContact.setPhone("999999999");
		
		emergencyContact = emergencyContactService.save(emergencyContact);
		
		Assert.isTrue(emergencyContactService.findAll().contains(emergencyContact));
		Assert.isTrue(explorer.getEmergencyContacts().contains(emergencyContact));
		
		this.unauthenticate();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSaveBadAuth(){
		this.authenticate("Auditor1");
		
		UserAccount principal;
		Explorer explorer;
		EmergencyContact emergencyContact;
		
		principal = LoginService.getPrincipal();
		emergencyContact = emergencyContactService.create();
		emergencyContact.setEmail("email@email.com");
		emergencyContact.setName("name");
		emergencyContact.setPhone("999999999");
		
		emergencyContact = emergencyContactService.save(emergencyContact);
		
		explorer = (Explorer) actorService.findByUserAccountId(principal.getId());
		
		Assert.isTrue(emergencyContactService.findAll().contains(emergencyContact));
		Assert.isTrue(explorer.getEmergencyContacts().contains(emergencyContact));
		
		this.unauthenticate();
	}
	
	@Test
	public void testDeleteAuth(){
		this.authenticate("Explorer1"); //Se sobreentiende que es un Explorer para evitar alargar el m�todo innecesariamente
		
		UserAccount principal;
		Explorer explorer;
		EmergencyContact emergencyContact;
		
		principal = LoginService.getPrincipal();
		explorer = (Explorer) actorService.findByUserAccountId(principal.getId());
		emergencyContact = emergencyContactService.create();
		emergencyContact.setEmail("email@email.com");
		emergencyContact.setName("name");
		emergencyContact.setPhone("999999999");
		
		emergencyContact = emergencyContactService.save(emergencyContact);
		
		Assert.isTrue(emergencyContactService.findAll().contains(emergencyContact));
		Assert.isTrue(explorer.getEmergencyContacts().contains(emergencyContact));
		
		emergencyContactService.delete(emergencyContact);
		
		Assert.isTrue(!emergencyContactService.findAll().contains(emergencyContact));
		Assert.isTrue(!explorer.getEmergencyContacts().contains(emergencyContact));
		
		this.unauthenticate();
	}
	
	@Test
	public void testDeleteBadAuth(){
		this.authenticate("Explorer1"); //Se sobreentiende que es un Explorer para evitar alargar el m�todo innecesariamente
		
		UserAccount principal;
		Explorer explorer;
		EmergencyContact emergencyContact;
		
		principal = LoginService.getPrincipal();
		emergencyContact = emergencyContactService.create();
		emergencyContact.setEmail("email@email.com");
		emergencyContact.setName("name");
		emergencyContact.setPhone("999999999");
		
		emergencyContact = emergencyContactService.save(emergencyContact);
		explorer = (Explorer) actorService.findByUserAccountId(principal.getId());
		
		Assert.isTrue(emergencyContactService.findAll().contains(emergencyContact));
		Assert.isTrue(explorer.getEmergencyContacts().contains(emergencyContact));
		
		emergencyContactService.delete(emergencyContact);
		
		Assert.isTrue(!emergencyContactService.findAll().contains(emergencyContact));
		Assert.isTrue(!explorer.getEmergencyContacts().contains(emergencyContact));
		
		this.unauthenticate();
	}
}

<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<display:table name="explorers" id="row" requestURI="${requestURI}"
	pagesize="5">

	<spring:message code="explorer.edit.name" var="explorerName" />
	<display:column property="name" title="${explorerName}" sortable="true" />

	<spring:message code="explorer.edit.surname" var="explorerSurname" />
	<display:column property="surname" title="${explorerSurname}" sortable="true" />

	<spring:message code="explorer.edit.email" var="explorerEmail" />
	<display:column property="email" title="${explorerEmail}" sortable="false" />
	
	<spring:message code="explorer.edit.phone" var="explorerPhone" />
	<display:column property="phone" title="${explorerPhone}" sortable="false" />

	<spring:message code="explorer.list.emergencycontact" var="emergencycontactList" />
	<display:column title="" sortable="false">
			<spring:url
				value="emergencyContact/list.do?explorerId=${row.getId()}"
				var="editLink" />
			<a href="${editLink}"> 
				<jstl:out value="${emergencycontactList}" />
			</a>
	</display:column>
</display:table>

<spring:message code="explorer.list.back" var="listBack" />
<input type="button" value="${listBack}"
	onClick="javascript:relativeRedir('trip/list.do');" />
	



<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="trip/manager/cancel.do" modelAttribute="trip">
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="ticker" />
	<form:hidden path="manager" />
	<form:hidden path="tags" />
	<form:hidden path="stages" />
	<form:hidden path="title"/>
	<form:hidden path="description"/>
	<form:hidden path="requirements"/>
	<form:hidden path="publicationDate"/>
	<form:hidden path="endDate"/>
	<form:hidden path="startDate"/>
	<form:hidden path="ranger"/>
	<form:hidden path="category"/>
	<form:hidden path="legalText"/>
	
	<form:label path="cancellationReason">
		<spring:message code="trip.cancel.cancellation" />
	</form:label>
	<form:textarea path="cancellationReason" style="width:200px; heigth:40px"/>
	<form:errors path="cancellationReason" cssClass="error" />
	
	
	<br/>
	<spring:message code= "trip.create.save" var="saveButton"/>
	<input type="submit" name="save"
		value="${saveButton}" />
	
	<spring:message code= "backText" var="back"/>
	<input type="button" name="back"
		value="${back}"
		onClick="javascript: relativeRedir('trip/manager/list.do');" />
</form:form>
<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 
<jsp:useBean id="currentDate" class="java.util.Date" />
<security:authentication property="principal.username" var="principal"/>
<display:table name="stages" id="row" requestURI="stage/manager/list.do" pagesize="5">
	
	<spring:message code="stage.list.titleHeader" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />
	
	<spring:message code="stage.list.descriptionHeader" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" sortable="false" />
	
	<spring:message code="stage.list.price" var="priceHeader" />
	<display:column property="price" title="${priceHeader}" sortable="true" />
	
	<security:authorize access="hasRole('MANAGER')">
		<jstl:if test="${trip.getPublicationDate().after(currentDate)}">
			<spring:message code="stage.list.delete" var="delete"/>
			<display:column title="" sortable="false">
						<spring:url value="stage/manager/delete.do?tripId=${trip.getId()}&stageId=${row.getId()}" var="deleteStage"/>
						<a href="${deleteStage}">
							<jstl:out value="${delete}"/>
						</a>
			</display:column>
		</jstl:if>
	</security:authorize>
	
	
</display:table>

<spring:message code="stage.list.back" var="Back"/>
<input type="button" value="${Back}" 
	onClick="javascript: relativeRedir('trip/manager/display.do?tripId=${trip.getId()}');" />
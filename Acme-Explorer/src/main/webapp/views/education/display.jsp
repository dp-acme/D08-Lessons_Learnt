<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- TITLE -->
<spring:message code="education.display.title" />
:
<jstl:out value="${education.title}"></jstl:out>

<br />
<br />
<!-- ATTACHMENT -->
<spring:message code="education.display.attachment" />
:
<jstl:out value="${education.attachment}"></jstl:out>
&nbsp;
<!-- INSTITUTION -->
<spring:message code="education.display.institution" />
:
<jstl:out value="${education.institution}"></jstl:out>

<br />
<br />
<!-- COMMENTS -->
<spring:message code="education.display.comments" />
:
<jstl:out value="${education.comments}"></jstl:out>
&nbsp;
<!-- STUDIES START -->
<spring:message code="education.display.studiesStart" />
:
<jstl:out value="${education.studiesStart}"></jstl:out>

<br />
<br />
<!-- STUDIES END -->
<spring:message code="education.display.studiesEnd" />
:
<jstl:out value="${education.studiesEnd}"></jstl:out>

<br />
<br />
<!-- IF WE ARE AUTHENTICATE DIFERENT URL TO BACK -->
<security:authorize access="hasRole('RANGER')">
<spring:url var="urlDisplayEducation" value="education/ranger/list.do" />
</security:authorize>

<security:authorize access="isAnonymous()">
<spring:url var="urlDisplayEducation" value="education/list.do?curriculumId=${curriculumId}" />
</security:authorize>

<!-- BACK -->
<input type="button" name="back"
	value="<spring:message code="education.display.back" />"
	onclick="javascript: relativeRedir('${urlDisplayEducation}');" />


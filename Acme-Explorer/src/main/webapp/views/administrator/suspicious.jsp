<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<h2><spring:message code="administrator.suspicious.ranger" /></h2>

<spring:message code="administrator.suspicious.name" var="name"/>
<spring:message code="administrator.suspicious.account" var="account"/>
<spring:message code="administrator.suspicious.access" var="access"/>

<display:table requestURI="administrator/administrator/suspicious.do" name="rangers" id="row"> 
	<display:column title="${name}" property="name"/>
	<display:column title="${account}" property="userAccount.username"/>
	<display:column title="${access}">
		<jstl:if test="${row.getUserAccount().isAccountNonLocked()}">
			<a href="administrator/administrator/ban.do?actorId=${row.getId()}">
				<spring:message code="administrator.suspicious.ban" />
			</a>
		</jstl:if>
		
		<jstl:if test="${!row.getUserAccount().isAccountNonLocked()}">
			<a href="administrator/administrator/unban.do?actorId=${row.getId()}">
				<spring:message code="administrator.suspicious.unban" />
			</a>
		</jstl:if>
	</display:column>
</display:table>

<h2><spring:message code="administrator.suspicious.manager" /></h2>

<display:table requestURI="administrator/administrator/suspicious.do" name="managers" id="row"> 
	<display:column title="${name}" property="name"/>
	<display:column title="${account}" property="userAccount.username"/>
	<display:column title="${access}">
		<jstl:if test="${row.getUserAccount().isAccountNonLocked()}">
			<a href="administrator/administrator/ban.do?actorId=${row.getId()}">
				<spring:message code="administrator.suspicious.ban" />
			</a>
		</jstl:if>
		
		<jstl:if test="${!row.getUserAccount().isAccountNonLocked()}">
			<a href="administrator/administrator/unban.do?actorId=${row.getId()}">
				<spring:message code="administrator.suspicious.unban" />
			</a>
		</jstl:if>
	</display:column>
</display:table>
<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<security:authorize access="isAnonymous()">
<jstl:set var="requestURI" value="miscellaneous/list.do" />
</security:authorize>
<security:authorize access="hasRole('RANGER')">
	<jstl:set var="requestURI" value="miscellaneous/ranger/list.do" />
</security:authorize>

<display:table name="miscellaneousRecords" id="row" requestURI="${requestURI}"
	pagesize="5">

	<spring:message code="miscellaneous.list.titleHeader" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="false" />

	<!-- Display -->
	<security:authorize access="hasRole('RANGER')">
		<spring:url var="urlDisplayMiscellaneous"
			value="miscellaneous/ranger/display.do?miscellaneousId=${row.getId()}" />
	</security:authorize>

	<security:authorize access="isAnonymous()">
		<spring:url var="urlDisplayMiscellaneous"
			value="miscellaneous/display.do?miscellaneousId=${row.getId()}" />
	</security:authorize>


	<display:column title="" sortable="false">
		<a href="<spring:url value="${urlDisplayMiscellaneous}" />"> <spring:message
				code="miscellaneous.list.display" />
		</a>
	</display:column>

	<!-- EDIT -->
	<display:column title="" sortable="false">
		<security:authorize access="hasRole('RANGER')">
			<a
				href="<spring:url value="miscellaneous/ranger/edit.do?miscellaneousId=${row.getId()}" />">
				<spring:message code="miscellaneous.list.edit" />
			</a>
		</security:authorize>
	</display:column>
</display:table>

<security:authorize access="hasRole('RANGER')">
	<a href="miscellaneous/ranger/create.do"><spring:message
			code="miscellaneous.create" /></a>
</security:authorize>


<!-- PROBLEMAS DE MOMENTO AHI, MIRAR -->
<!-- FOR BUTTON BACK -->
<!-- DIFFERENT URL FOR UNAUTHENTICATE AND AUTHENTICATE -->
<security:authorize access="hasRole('RANGER')">
	<spring:url var="urlDisplayCurriculum"
		value="curriculum/ranger/display.do?curriculumId=${row.getCurriculum().getId()}" />
</security:authorize>

<security:authorize access="isAnonymous()">
	<spring:url var="urlDisplayCurriculum"
		value="curriculum/display.do?curriculumId=${row.getCurriculum().getId()}" />
</security:authorize>

<input type="button" name="back"
	value="<spring:message code="miscellaneous.list.back" />"
	onclick="javascript: relativeRedir('${urlDisplayCurriculum}');" />


<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- TITLE -->
<spring:message code="miscellaneous.display.title" />:
<jstl:out value="${miscellaneous.title}"></jstl:out>
<br />
<br />

<!-- ATTACHMENT -->
<spring:message code="miscellaneous.display.attachment" />
:
<jstl:out value="${miscellaneous.attachment}"></jstl:out>
<br />
<br/>

<!-- COMMENTS -->
<spring:message code="miscellaneous.display.comments" />
:
<jstl:out value="${miscellaneous.comments}"></jstl:out>


<!-- IF WE ARE AUTHENTICATE DIFERENT URL TO BACK -->
<security:authorize access="hasRole('RANGER')">
<spring:url var="urlDisplayCurriculum" value="miscellaneous/ranger/list.do" />
</security:authorize>

<security:authorize access="isAnonymous()">
<spring:url var="urlDisplayCurriculum" value="miscellaneous/list.do?curriculumId=${curriculumId}" />
</security:authorize>
<br />
<br />
<!-- BACK -->
<input type="button" name="back"
	value="<spring:message code="miscellaneous.display.back" />"
	onclick="javascript: relativeRedir('${urlDisplayCurriculum}');" />


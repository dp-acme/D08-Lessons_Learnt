<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@page import="org.joda.time.DateTime"%>
<%@page import="java.util.Calendar"%>
<%@page import="domain.ApplyFor"%>
<%@page import="java.sql.Timestamp"%>

<%
	Timestamp date = new Timestamp(System.currentTimeMillis());
	Calendar datePlusMonth = Calendar.getInstance();
	Timestamp currentDatePlusMonth;
	datePlusMonth.add(Calendar.MONTH, 1);
	currentDatePlusMonth = new Timestamp(datePlusMonth.getTimeInMillis());
	request.setAttribute("currentDatePlusMonth", currentDatePlusMonth);
	request.setAttribute("date", date);
%>

<p>
	<spring:message code="applyfor.display.moment" />
	:
	<spring:message code="applyfor.dateFormatCode" var="datePattern" />
	<fmt:formatDate value="${application.getMoment()}"
		pattern="${datePattern}" />
</p>

<p>
	<spring:message code="applyfor.list.statusHeader" />
	:
	<jstl:set value="${application.getStatus().getStatus()}" var="status" />

	<jstl:choose>
		<jstl:when
			test="${status == \"PENDING\" && application.getTrip().getStartDate().after(currentDatePlusMonth)}">
			<span style="background-color: white"> <spring:message
					code="applyfor.status.pending" />
			</span>
		</jstl:when>
		<jstl:when test="${status == \"PENDING\"}">
			<span style="background-color: red"> <spring:message
					code="applyfor.status.pending" />
			</span>
		</jstl:when>
		<jstl:when test="${status == \"REJECTED\"}">
			<span style="background-color: gray"> <spring:message
					code="applyfor.status.rejected" />
			</span>
		</jstl:when>
		<jstl:when test="${status == \"DUE\"}">
			<span style="background-color: yellow"> <spring:message
					code="applyfor.status.due" />
			</span>
		</jstl:when>
		<jstl:when test="${status == \"ACCEPTED\"}">
			<span style="background-color: green"> <spring:message
					code="applyfor.status.accepted" />
			</span>
		</jstl:when>
		<jstl:when test="${status == \"CANCELLED\"}">
			<span style="background-color: cyan"> <spring:message
					code="applyfor.status.cancelled" />
			</span>
		</jstl:when>
	</jstl:choose>
</p>

<p>
	<spring:message code="applyfor.list.commentsHeader" />
	:
	<jstl:out value="${application.getComments()}" />
</p>

<jstl:if test="${application.getRejectedReason() != null}">
	<p>
		<spring:message code="applyfor.display.rejectedReason" />
		:
		<jstl:out value="${application.getRejectedReason()}" />
	</p>
</jstl:if>

<security:authorize access="hasRole('EXPLORER')">
	<jstl:if test="${application.getCreditCard() != null}">
		<p>
			<b><spring:message code="applyfor.edit.enterCreditCard.title" />:</b><br />
			<spring:message code="applyfor.edit.enterCreditCard.holderName" />
			:
			<jstl:out value="${application.getCreditCard().getHolderName()}" />
			<br />
			<spring:message code="applyfor.edit.enterCreditCard.brandName" />
			:
			<jstl:out value="${application.getCreditCard().getBrandName()}" />
			<br />
			<spring:message code="applyfor.edit.enterCreditCard.number" />
			:
			<jstl:out value="${application.getCreditCard().getNumber()}" />
			<br />
			<spring:message code="applyfor.edit.enterCreditCard.expirationMonth" />
			:
			<jstl:out value="${application.getCreditCard().getExpirationMonth()}" />
			<br />
			<spring:message code="applyfor.edit.enterCreditCard.expirationYear" />
			:
			<jstl:out value="${application.getCreditCard().getExpirationYear()}" />
			<br />
		</p>
	</jstl:if>
</security:authorize>


<security:authorize access="hasRole('EXPLORER')">
	<input type="button"
		value="<spring:message code="applyfor.list.back" />"
		onClick="javascript: relativeRedir('applyfor/explorer/list.do');" />
</security:authorize>

<security:authorize access="hasRole('MANAGER')">
	<input type="button"
		value="<spring:message code="applyfor.list.back" />"
		onClick="javascript: relativeRedir('applyfor/manager/list.do');" />
</security:authorize>

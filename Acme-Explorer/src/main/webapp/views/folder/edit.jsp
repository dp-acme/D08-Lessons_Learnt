<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<form:form method="POST" action="folder/edit.do" modelAttribute="folder">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="type" />
	<form:hidden path="actor" />
	<form:hidden path="messages" />
	<jstl:if test="${folder.getId() == 0}" >
		<form:hidden path="parentFolder" />
	</jstl:if>
	
	<form:label path="name">
		<spring:message code="folder.edit.name"/>
	</form:label>
	<form:input path="name" />
	<form:errors cssClass="error" path="name" />
	
	<br />
	
	<jstl:if test="${folder.getId() != 0}" >
		<form:label path="parentFolder">
			<spring:message code="folder.edit.parentFolder"/>
		</form:label>
		<form:select path="parentFolder">
			<form:option label="----" value="0" />
			<form:options items="${parentFolders}" itemLabel="name" itemValue="id" />
		</form:select>
		<form:errors cssClass="error" path="parentFolder" />
	</jstl:if>
	
	<br />
	<br />
	
	<spring:message code="folder.edit.save" var="editSave" />
	<input type="submit" name="save" value="${editSave}" />
	
	<jstl:if test="${folder.getId() != 0}">
		<spring:message code="folder.edit.delete" var="editDelete" />
		<input type="submit" name="delete" value="${editDelete}" 
			onClick="javascript: return confirm('<spring:message code="folder.edit.deleteConfirm" />');" />
	</jstl:if>
	
	<jstl:set value="" var="backFolderIdParam" />
	<jstl:if test="${folder.getId() != 0}">
		<jstl:set value="?folderId=${folder.getId()}" var="backFolderIdParam" />
	</jstl:if>
	<jstl:if test="${folder.getId() == 0 && folder.getParentFolder() != null}">
		<jstl:set value="?folderId=${folder.getParentFolder().getId()}" var="backFolderIdParam" />
	</jstl:if>
	<spring:message code="folder.edit.cancel" var="editCancel"/>
	<input type="button" value="${editCancel}" onClick="javascript: relativeRedir('folder/list.do${backFolderIdParam}');" />
	
</form:form>
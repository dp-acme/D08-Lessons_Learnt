package controllers.note;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.NoteService;
import domain.Note;

@Controller
@RequestMapping("/note/manager")
public class NoteManagerController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private NoteService noteService;

	// Constructors -----------------------------------------------------------

	public NoteManagerController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listByManager(@RequestParam(required=false) Integer tripId) {
		ModelAndView result;
		Collection<Note> notes;
		if(tripId!=null){
			notes=noteService.getNotesByTrip(tripId);
		}else{
			notes = noteService.getNotesByTripManager();
		}
		result = new ModelAndView("note/manager/list");
		result.addObject("notes", notes);
		result.addObject("requestURI", "note/manager/list.do");

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int noteId) {
		ModelAndView result;
		Note note;
		
		note = noteService.findOne(noteId);
		Assert.notNull(note);
		result = createEditModelAndView(note);

		return result;
	}


	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Note note, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(note);
		} else {
			try {
				noteService.save(note);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(note, "note.commit.error");
			}
		}
		return result;
	}

	protected ModelAndView createEditModelAndView(Note note) {
		ModelAndView result;

		result = createEditModelAndView(note, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Note note, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("note/manager/edit");
		result.addObject("note", note);

		result.addObject("message", messageCode);

		return result;
	}
}

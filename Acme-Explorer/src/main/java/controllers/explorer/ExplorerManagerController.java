/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.explorer;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ExplorerService;
import controllers.AbstractController;
import domain.Explorer;

@Controller
@RequestMapping("/explorer/manager")
public class ExplorerManagerController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ExplorerService explorerService;
	
	// Constructors -----------------------------------------------------------

	public ExplorerManagerController() {
		super();
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listByAuditor(@RequestParam int tripId) {
		ModelAndView result;
		Collection<Explorer> explorers;

			explorers = explorerService.findByTrip(tripId);	
			
		result = new ModelAndView("explorer/manager/list");
		result.addObject("explorers", explorers);
		result.addObject("requestURI", "explorer/manager/list.do");

		return result;
	}
}	
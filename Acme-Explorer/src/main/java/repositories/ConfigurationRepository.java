package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Configuration;

@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration, Integer>{
	
	@Query("select c from Configuration c") //Hacer un findAll simulado que te devuelva directamente un objeto
	public Configuration findConfiguration();
	
}

package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)

public class SurvivalClass extends DomainEntity {

	// Constructor
	
	public SurvivalClass() {
		super();
	}
	
	// Attributes
	
	private String title;
	private String description;
	private Date moment;
	private Coordinates location;
	
	@NotBlank
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@NotBlank
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}
	public void setMoment(Date moment) {
		this.moment = moment;
	}
	
	@NotNull
	@Valid
	public Coordinates getLocation() {
		return location;
	}
	public void setLocation(Coordinates location) {
		this.location = location;
	}
	
	// Relationships
	
	private Trip trip;
	private Collection<Explorer> explorers;
	
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Trip getTrip() {
		return trip;
	}
	public void setTrip(Trip trip) {
		this.trip = trip;
	}
	
	@NotNull
	@Valid
	@ManyToMany
	public Collection<Explorer> getExplorers() {
		return explorers;
	}
	public void setExplorers(Collection<Explorer> explorers) {
		this.explorers = explorers;
	}
	
}
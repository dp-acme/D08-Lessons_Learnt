package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Finder extends DomainEntity{

	// Constructor
	
	public Finder() {
		super();
	}
	
	// Attributes
	
	private String keyWord;
	private Date dateStart;
	private Date dateEnd;
	private Double priceStart;
	private Double priceEnd;
	private Date lastSaved;

	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateEnd() {
		return dateEnd;
	}
	
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	@Range(min=0)
	@Digits(integer=99,fraction=2)
	public Double getPriceStart() {
		return priceStart;
	}
	public void setPriceStart(Double priceStart) {
		this.priceStart = priceStart;
	}

	@Range(min=0)
	@Digits(integer=99,fraction=2)
	public Double getPriceEnd() {
		return priceEnd;
	}
	public void setPriceEnd(Double priceEnd) {
		this.priceEnd = priceEnd;
	}
	
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastSaved() {
		return lastSaved;
	}
	public void setLastSaved(Date dateStart) {
		this.lastSaved = dateStart;
	}
	
	// Relationships
	
	private Explorer explorer;
	private Collection<Trip> trips;
	
	@Valid
	@OneToMany
	public Collection<Trip> getTrips() {
		return trips;
	}
	public void setTrips(Collection<Trip> trips) {
		this.trips = trips;
	}
	
	@Valid
	@OneToOne
	public Explorer getExplorer() {
		return  this.explorer;
	}
	
	public void setExplorer(Explorer explorer) {
		this.explorer = explorer;
	}
	
}

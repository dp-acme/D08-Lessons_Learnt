package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CurriculumRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Curriculum;
import domain.Education;
import domain.Endorser;
import domain.Miscellaneous;
import domain.Professional;
import domain.Ranger;

@Service
@Transactional
public class CurriculumService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private CurriculumRepository curriculumRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;

	@Autowired
	private RangerService rangerService;
	
	@Autowired
	private MiscellaneousService miscellaneousService;
	
	@Autowired
	private EndorserService endorserService;
	
	@Autowired
	private ProfessionalService professionalService;
	
	@Autowired
	private EducationService educationService;
	

	// Constructor ---------------------------------------------------

	public CurriculumService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Curriculum create() {
		Curriculum result;
		UserAccount principal;
		Actor actor;
		Authority auth;
		Ranger ranger;

		result = new Curriculum();
		principal = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(principal.getId());
		
		auth = new Authority();
		auth.setAuthority(Authority.RANGER);
		Assert.isTrue(actor.getUserAccount().getAuthorities().contains(auth));
		ranger = (Ranger)actor;
		
		result.setRanger(ranger);
		result.setEducationRecords(new ArrayList<Education>());
		result.setEndorserRecords(new ArrayList<Endorser>());
		result.setMiscellaneousRecords(new ArrayList<Miscellaneous>());
		result.setProfessionalRecords(new ArrayList<Professional>());
				
		result.setTicker(TripService.generateTicker(result.getId())); //Generar ticker con id generado por el primer save
		
		return result;
	}

	public Curriculum findOne(int curriculumId) {
		Assert.isTrue(curriculumId != 0);

		Curriculum result;

		result = curriculumRepository.findOne(curriculumId);

		return result;
	}

	public Collection<Curriculum> findAll() {
		Collection<Curriculum> result;

		result = curriculumRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Curriculum save(Curriculum curriculum) {
		Assert.notNull(curriculum);
		
		UserAccount principal;
		Curriculum result;

		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));

		result = curriculumRepository.save(curriculum);
		
		if(curriculum.getId() == 0){
			Ranger ranger;
			
			ranger = curriculum.getRanger();
			ranger.setCurriculum(result);
			
			rangerService.save(ranger);
		}
		
		return result;
	}

	public void delete(Curriculum curriculum) {
		Assert.notNull(curriculum);
		
		UserAccount principal;
		Ranger ranger; 

		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		Assert.isTrue(curriculumRepository.exists(curriculum.getId()));	
		
		ranger = rangerService.findOne(curriculum.getRanger().getId());
		ranger.setCurriculum(null);
		rangerService.save(ranger);
		
		miscellaneousService.deleteAllRecordsOfCurriculum(curriculum);
		educationService.deleteAllRecordsOfCurriculum(curriculum);
		endorserService.deleteAllRecordsOfCurriculum(curriculum);
		professionalService.deleteAllRecordsOfCurriculum(curriculum);

		curriculumRepository.delete(curriculum);

	}

	// Other business methods ---------------------------------------------------

	public Collection<String> getCurriculumTickers(){
		Collection<String> result;
		
		result = curriculumRepository.getCurriculumTickers();
		
		return result;
	}
	
}

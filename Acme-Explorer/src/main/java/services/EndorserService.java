package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.EndorserRepository;
import security.LoginService;
import security.UserAccount;
import domain.Curriculum;
import domain.Endorser;

@Service
@Transactional
public class EndorserService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private EndorserRepository endorserRepository;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private CurriculumService curriculumService;

	// Constructor ---------------------------------------------------
	
	public EndorserService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Endorser create(Curriculum curriculum) {
		UserAccount principal;
		Endorser result;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		
		result = new Endorser();
		result.setCurriculum(curriculum);
		
		return result;
	}
	
	public Endorser findOne(int endorserId) {
		Assert.isTrue(endorserId != 0);
		
		Endorser result;
		
		result = endorserRepository.findOne(endorserId);
		
		return result;
	}
	
	public Collection<Endorser> findAll() {		
		Collection<Endorser> result;
		
		result = endorserRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Endorser save(Endorser endorser) {
		Assert.notNull(endorser);
		
		UserAccount principal;
		Endorser result;
		Curriculum curriculum;
		
		curriculum = endorser.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(endorser.getCurriculum().getRanger().getUserAccount().equals(principal));
		
		result = endorserRepository.save(endorser);

		if (endorser.getId() == 0) {
			curriculum.getEndorserRecords().add(result);
			curriculumService.save(curriculum);
		}
		
		return result;
	}
	
	public void delete(Endorser endorser) {
		Assert.notNull(endorser);
		
		UserAccount principal;
		Curriculum curriculum;
		
		curriculum = endorser.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		Assert.isTrue(endorserRepository.exists(endorser.getId()));
		
		curriculum.getEndorserRecords().remove(endorser);
		curriculumService.save(curriculum);
		
		endorserRepository.delete(endorser);
	}
	
	// Other business methods ---------------------------------------------------
	public void deleteAllRecordsOfCurriculum(Curriculum c){
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(c.getRanger().getUserAccount().equals(principal));

		endorserRepository.deleteInBatch(c.getEndorserRecords());
	}

}

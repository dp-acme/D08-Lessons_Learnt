package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Record;

import repositories.RecordRepository;

@Service
@Transactional
public class RecordService {
	
	// Managed repository --------------------------------------
	
	@Autowired
	private RecordRepository recordRepository;
	
	// Supporting services -------------------------------------
	
	
	
	// Constructors --------------------------------------------
	
	public RecordService(){
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public Collection<Record> findAll() {
		Collection<Record> result;
		
		result = recordRepository.findAll();
				
		return result;
	}

	public Record findOne(int recordId) {
		Record result;
		
		result = recordRepository.findOne(recordId);
		
		Assert.notNull(result);				
		
		return result;
	}
	
	public Record save(Record record){
		Assert.notNull(record);	
		
		Record result;
		
		result = recordRepository.save(record);
		
		return result;
	}
	
	public void delete(Record record){
		Assert.notNull(record);
		
		Assert.isTrue(recordRepository.exists(record.getId()));
		
		recordRepository.delete(record);
	}
	
	// Other business methods -----------------------------------
	
	
	
}

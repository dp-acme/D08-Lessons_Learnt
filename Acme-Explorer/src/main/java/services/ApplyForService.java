package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ApplyForRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.ApplyFor;
import domain.Explorer;
import domain.Status;
import domain.Trip;

@Service
@Transactional
public class ApplyForService {
	
	// Managed repository --------------------------------------
	
	@Autowired
	private ApplyForRepository applyForRepository;
	
	// Supporting services -------------------------------------
	@Autowired
	private ActorService actorService;
	@Autowired
	private ExplorerService explorerService;
	@Autowired
	private MessageService messageService;
	
	// Constructor -------------------------------------
	
	public ApplyForService() {
		super();
	}
	
	// CRUD methods ------------------------------------
	
	public ApplyFor create(Trip trip) {
		UserAccount userAccount;
		Actor actor;

		userAccount = LoginService.getPrincipal();
		actor = actorService.findByUserAccountId(userAccount.getId());
		Assert.isInstanceOf(Explorer.class, actor);
		
		ApplyFor result;
		Status status;

		result = new ApplyFor();
		status = new Status();
		status.setStatus(Status.PENDING);
		result.setTrip(trip);
		result.setMoment(new Date(System.currentTimeMillis() - 1));
		result.setStatus(status);
		result.setExplorer((Explorer)actor);
		
		return result;
	}
	
	public Collection<ApplyFor> findAll() {
		Collection<ApplyFor> result;
		
		result = applyForRepository.findAll();

		return result;
	}

	public ApplyFor findOne(int applyForId) {
		Assert.isTrue(applyForId != 0);		

		ApplyFor result;		
		
		result = applyForRepository.findOne(applyForId);
		
		return result;
	}
	
	//Este m�todo no puede ser usado para cambiar el Status a REJECTED o a DUE (Usar changeStatus para eso)
	public ApplyFor save(ApplyFor applyFor){
		Assert.notNull(applyFor);
		
		UserAccount userAccount;
		ApplyFor result;
		Status due;
		Status accepted;
		Status cancelled;
		
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(applyFor.getExplorer().getUserAccount().equals(userAccount) || applyFor.getTrip().getManager().getUserAccount().equals(userAccount));
		
		due = new Status();
		accepted = new Status();
		cancelled = new Status();
		due.setStatus(Status.DUE);
		accepted.setStatus(Status.ACCEPTED);
		cancelled.setStatus(Status.CANCELLED);
		
		if (applyFor.getId() == 0) {
			applyFor.setMoment(new Date(System.currentTimeMillis() - 1));
			
		} else {
			ApplyFor applyForOld;
			
			applyForOld = applyForRepository.findOne(applyFor.getId());
			
			if(applyFor.getStatus().getStatus().equals(Status.CANCELLED)){
				Assert.isTrue(applyFor.getTrip().getStartDate().after(new Date()));
				messageService.sendStatusNotification(applyFor);
			}else{
			
			if (!applyFor.getStatus().getStatus().equals(Status.REJECTED) && !applyFor.getCreditCard().equals(applyForOld.getCreditCard())) {
				Assert.isTrue(applyForOld.getStatus().getStatus().equals(Status.DUE));
				
				applyFor.setStatus(accepted);
				
				messageService.sendStatusNotification(applyFor);
				
			}
			
			Assert.notNull(applyFor.getCreditCard().getBrandName());
			Assert.notNull(applyFor.getCreditCard().getHolderName());
			Assert.notNull(applyFor.getCreditCard().getExpirationMonth());
			Assert.notNull(applyFor.getCreditCard().getExpirationYear());
			Assert.notNull(applyFor.getCreditCard().getNumber());
		}}
		
		result = applyForRepository.save(applyFor);
		
		if (applyFor.getId() == 0) {
			Explorer explorer;
			
			explorer = applyFor.getExplorer();
			explorer.getApplications().add(result);
			
			explorerService.save(explorer);
		}
		
		return result;
	}
	
	// Other methods ------------------------------------
	
	//Este m�todo se usa para cambiar el Status de un ApplyFor a DUE o REJECTED
	public ApplyFor changeStatus(ApplyFor applyFor) {
		Assert.notNull(applyFor);
		
		UserAccount userAccount;
		ApplyFor result;
		
		userAccount = LoginService.getPrincipal();
		
		Assert.isTrue(applyFor.getTrip().getManager().getUserAccount().equals(userAccount));
		
		Assert.isTrue((applyFor.getStatus().getStatus().equals(Status.DUE) || applyFor.getStatus().getStatus().equals(Status.REJECTED)));
		
		messageService.sendStatusNotification(applyFor);
		
		result = applyForRepository.save(applyFor);
		
		return result;  
	}	
	
	public Map<Status, Collection<ApplyFor>> getApplicationsGroupedByStatus(Explorer explorer) {
		Assert.notNull(explorer);
		
		Map<Status, Collection<ApplyFor>> result; 
		List<Status> status;
		
		result = new HashMap<Status, Collection<ApplyFor>>();
		status = new ArrayList<Status>(Status.listStatus());
		
		for(Status s : status) {
			result.put(s, applyForRepository.getApplicationsFromExplorerAndStatus(explorer.getId(), s));
		}
		
		return result;
	}
	
	public Collection<ApplyFor> getApplicationsFromExplorerAndStatus(Explorer explorer, Status status) {
		Assert.notNull(explorer);
		Assert.notNull(status);
		
		Collection<ApplyFor> result;
		
		result = applyForRepository.getApplicationsFromExplorerAndStatus(explorer.getId(), status);
		
		return result;		
	}
	
	public ApplyFor getApplicationFromExplorerAndTrip(Explorer explorer, Trip trip) {
		Assert.notNull(explorer);
		Assert.notNull(trip);
		
		ApplyFor result;
		
		result = applyForRepository.getApplicationFromExplorerAndTrip(explorer.getId(), trip.getId());
		
		return result;		
	}

	public Collection<ApplyFor> filterApplicationsByStatus(Status status){
		Collection<ApplyFor> result;
		
		result = applyForRepository.filterApplicationsByStatus(status);
		
		return result;
	}

	public Collection<ApplyFor> filterApplicationsByDifferentStatus(Status status){
		Collection<ApplyFor> result;
		
		result = applyForRepository.filterApplicationsByDifferentStatus(status);
		
		return result;
	}
	
	public Collection<ApplyFor> getApplicationsByTrip(int tripId){
		Collection<ApplyFor> result;
		
		result = applyForRepository.getApplicationsByTrip(tripId);
		
		return result;
	}
}

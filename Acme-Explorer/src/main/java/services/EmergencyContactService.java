
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.EmergencyContactRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.EmergencyContact;
import domain.Explorer;

@Service
@Transactional
public class EmergencyContactService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private EmergencyContactRepository	emergencyContactRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService				actorService;

	@Autowired
	private ExplorerService				explorerService;


	// Constructor ---------------------------------------------------

	public EmergencyContactService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public EmergencyContact create() {
		EmergencyContact result;

		result = new EmergencyContact();

		return result;
	}

	public Collection<EmergencyContact> findAll() {
		Collection<EmergencyContact> result;

		result = this.emergencyContactRepository.findAll();

		return result;
	}

	public EmergencyContact findOne(final int emergencyContactId) {
		Assert.isTrue(emergencyContactId != 0);

		EmergencyContact result;

		result = this.emergencyContactRepository.findOne(emergencyContactId);

		return result;
	}

	public EmergencyContact save(final EmergencyContact emergencyContact) {
		Assert.notNull(emergencyContact);

		Authority auth;
		UserAccount principal;
		EmergencyContact result;
		Explorer explorer;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.EXPLORER);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		explorer = (Explorer) this.actorService.findByUserAccountId(principal.getId());

		if (emergencyContact.getId() != 0)
			Assert.isTrue(explorer.getEmergencyContacts().contains(emergencyContact));

		Assert.isTrue((emergencyContact.getPhone() != null && !emergencyContact.getPhone().equals("")) || (emergencyContact.getEmail() != null && !emergencyContact.getEmail().equals("")));
		if (emergencyContact.getPhone() == null)
			Assert.hasText(emergencyContact.getEmail());
		else if (emergencyContact.getEmail() == null)
			Assert.hasText(emergencyContact.getPhone());

		result = this.emergencyContactRepository.save(emergencyContact);

		if (emergencyContact.getId() == 0) {
			explorer.getEmergencyContacts().add(result);

			this.explorerService.save(explorer);
		}

		return result;
	}

	public void delete(final EmergencyContact emergencyContact) {
		Assert.notNull(emergencyContact);
		Assert.isTrue(this.emergencyContactRepository.exists(emergencyContact.getId()));

		Collection<Explorer> explorers;

		explorers = this.explorerService.findAll();

		for (final Explorer e : explorers)
			if (e.getEmergencyContacts().contains(emergencyContact)) {
				e.getEmergencyContacts().remove(emergencyContact);

				this.explorerService.save(e);
			}

		this.emergencyContactRepository.delete(emergencyContact);
	}
}

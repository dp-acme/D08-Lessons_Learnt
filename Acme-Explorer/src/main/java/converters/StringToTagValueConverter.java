package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.TagValueRepository;
import domain.TagValue;

@Component
@Transactional
public class StringToTagValueConverter implements Converter<String, TagValue>{

	@Autowired TagValueRepository tagValueRepository;
	
	@Override
	public TagValue convert(String source) {
		TagValue result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = tagValueRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
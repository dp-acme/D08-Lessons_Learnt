package converters;

import java.net.URLEncoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.FolderType;

@Component
@Transactional
public class FolderTypeToStringConverter implements Converter<FolderType, String>{

	@Override
	public String convert(FolderType source) {
		String result; 
		StringBuilder builder;
		
		if(source == null){
			result = null;
		}else{
			try{
				builder = new StringBuilder();
				builder.append(URLEncoder.encode(source.getFolderType(), "UTF-8"));
				result = builder.toString();
			}catch(Throwable oops){
				throw new IllegalArgumentException(oops);
			}
		}
		return result;
	}

}

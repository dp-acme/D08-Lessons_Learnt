
package converters;

import java.net.URLDecoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Priority;

@Component
@Transactional
public class StringToPriorityConverter implements Converter<String, Priority> {

	@Override
	public Priority convert(final String source) {
		Priority result;
		String parts[];

		if (source == null)
			result = null;
		else
			try {
				parts = source.split("\\|");
				result = new Priority();
				result.setPriority(URLDecoder.decode(parts[0], "UTF-8"));
			} catch (final Throwable oops) {
				throw new IllegalArgumentException(oops);
			}
		return result;
	}

}

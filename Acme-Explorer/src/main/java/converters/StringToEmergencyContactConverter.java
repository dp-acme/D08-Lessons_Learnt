package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.EmergencyContactRepository;

import domain.EmergencyContact;

@Component
@Transactional
public class StringToEmergencyContactConverter implements Converter<String, EmergencyContact>{

	@Autowired EmergencyContactRepository emergencyContactRepository;
	
	@Override
	public EmergencyContact convert(String source) {
		EmergencyContact result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = emergencyContactRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}

package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.TagKey;

@Component
@Transactional
public class TagKeyToStringConverter implements Converter<TagKey, String> {

	@Override
	public String convert(TagKey source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}

}

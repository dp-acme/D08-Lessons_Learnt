package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.TagKeyRepository;
import domain.TagKey;

@Component
@Transactional
public class StringToTagKeyConverter implements Converter<String, TagKey>{

	@Autowired TagKeyRepository tagKeyRepository;
	
	@Override
	public TagKey convert(String source) {
		TagKey result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = tagKeyRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}

<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:set var="uri" value="/announcement/list.do" />

<jstl:if test="${rendezvousId != null}">
	<jstl:set var="uri" value="/announcement/list.do?rendezvousId=${rendezvousId}" />
</jstl:if>

<display:table name="announcements" id="row" requestURI="${uri}" pagesize="5">
	<display:column>
		<jstl:out value="${row.getTitle()}" />
		
		<br />
		
		<spring:message code="announcement.formatDate" var="formatDate" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${formatDate}" />
		
		<br />
		
		<a href="/rendezvous/display.do?rendezvousId=${row.getRendezvous().getId()}"><jstl:out value="${row.getRendezvous().getName()}" /></a>
		
		<br />
		<br />
		
		<jstl:out value="${row.getDescription()}" />
	</display:column>
</display:table>

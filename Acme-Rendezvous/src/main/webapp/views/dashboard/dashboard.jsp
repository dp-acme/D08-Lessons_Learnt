<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page import="java.text.DecimalFormat" %>

<h2><spring:message code="dashboard.statistics" /></h2>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioCreatorsOverNonCreators" />:</b>
		<jstl:out value="${ratioCreatorsOverNonCreators}" />
	</li>
</ul>

<table> 
	<tr style="background-color: yellow;">
		<th><spring:message code="dashboard.table.property" /></th>
		<th><spring:message code="dashboard.table.average" /></th>
		<th><spring:message code="dashboard.table.sd" /></th>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdRendezvousPerUser" /></td>
		<td><jstl:out value="${avgSdRendezvousPerUser[0]}" /></td>
		<td><jstl:out value="${avgSdRendezvousPerUser[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdUsersPerRendezvous" /></td>
		<td><jstl:out value="${avgSdUsersPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdUsersPerRendezvous[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdRSVPsPerUser" /></td>
		<td><jstl:out value="${avgSdRSVPsPerUser[0]}" /></td>
		<td><jstl:out value="${avgSdRSVPsPerUser[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdAnnoucementsPerRendezvous" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdRepliesPerComment" /></td>
		<td><jstl:out value="${avgSdRepliesPerComment[0]}" /></td>
		<td><jstl:out value="${avgSdRepliesPerComment[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdQuestionsPerRendezvous" /></td>
		<td><jstl:out value="${avgSdQuestionsPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdQuestionsPerRendezvous[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.avgSdAnswersPerRendezvous" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[0]}" /></td>
		<td><jstl:out value="${avgSdAnnoucementsPerRendezvous[1]}" /></td>
	</tr>
</table>

<br>
<h2><spring:message code="dashboard.10RendezvousesOrderedByUsers" /></h2>

<spring:message code="dashboard.table.rendezvous.title" var="titleHeader" />
<spring:message code="dashboard.table.rendezvous.description" var="DescriptionHeader" />
<spring:message code="dashboard.table.rendezvous.moment" var="moment" />

<display:table id="row" name="10RendezvousesOrderedByUsers" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" title="${titleHeader}" sortable="true" />
	<display:column property="description" title="${DescriptionHeader}" sortable="true" />
	<display:column title="${momentHeader}" sortable="true" > 
		<spring:message code="dashboard.dateformat" var = "datePattern" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${datePattern}"/>
	</display:column>
</display:table>

<br>

<h2><spring:message code="dashboard.rendezvousWithOver75percentAnnoucements" /></h2>

<spring:message code="dashboard.table.rendezvous.title" var="titleHeader" />
<spring:message code="dashboard.table.rendezvous.description" var="DescriptionHeader" />
<spring:message code="dashboard.table.rendezvous.moment" var="moment" />

<display:table id="row" name="rendezvousWithOver75percentAnnoucements" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" title="${titleHeader}" sortable="true" />
	<display:column property="description" title="${DescriptionHeader}" sortable="true" />
	<display:column title="${momentHeader}" sortable="true" > 
		<spring:message code="dashboard.dateformat" var = "datePattern" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${datePattern}"/>
	</display:column>
</display:table>

<br>

<h2><spring:message code="dashboard.rendezvousLinkedOver10percentRendezvouses" /></h2>

<spring:message code="dashboard.table.rendezvous.title" var="titleHeader" />
<spring:message code="dashboard.table.rendezvous.description" var="DescriptionHeader" />
<spring:message code="dashboard.table.rendezvous.moment" var="moment" />

<display:table id="row" name="rendezvousLinkedOver10percentRendezvouses" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="name" title="${titleHeader}" sortable="true" />
	<display:column property="description" title="${DescriptionHeader}" sortable="true" />
	<display:column title="${momentHeader}" sortable="true" > 
		<spring:message code="dashboard.dateformat" var = "datePattern" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${datePattern}"/>
	</display:column>
</display:table>

<br>

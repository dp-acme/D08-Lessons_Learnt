<%--
 * display.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<p>
	<spring:message code="user.edit.name" var="name"/>
	<b><jstl:out value="${name}: "/></b>
	<jstl:out value="${user.getName()}" />
</p>

<p>
	<spring:message code="user.edit.surname" var="surname"/>
	<b><jstl:out value="${surname}: "/></b>
	<jstl:out value="${user.getSurname()}" />
</p>

<p>
	<spring:message code="user.edit.email" var="email"/>
	<b><jstl:out value="${email}: "/></b>
	<jstl:out value="${user.getEmail()}" />
</p>

<!-- <p> -->
<%-- 	<jstl:if test="${user.getAddress() != null && StringUtils.isBlank(user.getAddress()}"> --%>
<%-- 	<spring:message code="user.edit.address" var="address"/> --%>
<%-- 	<b><jstl:out value="${address}: "/></b> --%>
<%-- 	<jstl:out value="${user.getAddress()}" /> --%>
<%-- 	</jstl:if> --%>
<!-- </p> -->

<!-- <p> -->
<%-- 	<jstl:if test="${user.getPhone() != null && StringUtils.isBlank(user.getPhone()}"> --%>
<%-- 	<spring:message code="user.edit.phone" var="phone"/> --%>
<%-- 	<b><jstl:out value="${phone}: "/></b> --%>
<%-- 	<jstl:out value="${user.getPhone()}" /> --%>
<%-- 	</jstl:if> --%>
<!-- </p> -->


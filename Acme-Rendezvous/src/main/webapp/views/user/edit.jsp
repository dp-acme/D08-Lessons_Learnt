<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<p><spring:message code="customer.action.1" /></p>

<security:authorize access="hasRole('USER')">
			<spring:url value="user/user/edit.do" var="formEdit" />
</security:authorize>
<security:authorize access="isAnonymous()">
			<spring:url value="user/create.do" var="formEdit" />
</security:authorize>

<form:form action="${formEdit}" modelAttribute="user">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="myRendezvouses" />
	<form:hidden path="myRSVPs" />

	<security:authorize access="hasRole('USER')">
		<form:hidden path="userAccount"/>
	</security:authorize>
	
	<security:authorize access="isAnonymous()">
		<acme:textbox code="user.edit.username" path="userAccount.username"/>
		<acme:textbox code="user.edit.password" path="userAccount.password"/>

		<input type="hidden" name="userAccount.authorities" value="USER"/>
	</security:authorize>
		
	
	<acme:textbox code="user.edit.name" path="name"/>
	<acme:textbox code="user.edit.surname" path="surname"/>
	<acme:textbox code="user.edit.email" path="email"/>
	<acme:textbox code="user.edit.phone" path="phone"/>
	<acme:textbox code="user.edit.address" path="address"/>
	
	<acme:submit name="save" code="user.edit.save"/>
	<acme:cancel url="" code="user.edit.cancel"/>

</form:form>

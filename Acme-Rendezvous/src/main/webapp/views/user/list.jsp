<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<display:table name="users" id="row" requestURI="${requestUri}" pagesize="5">
	
	<spring:message code="user.list.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="true" />
	
	<spring:message code="user.list.surname" var="usernameHeader" />
	<display:column property="name" title="${usernameHeader}" sortable="true" />
	
	<spring:message code="user.list.profile" var="profileHeader" />
	<display:column title="" sortable="false">
			<spring:url value="user/display.do?userId=${row.getId()}"
				var="displayUser" />
			<a href="${displayUser}"> <jstl:out value="${profileHeader}" />
			</a>
	</display:column>
</display:table>



</html>
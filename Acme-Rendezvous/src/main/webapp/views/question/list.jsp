<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:set var="uri" value="/question/list.do" />

<jstl:if test="">
	<jstl:set var="uri" value="/question/list.do?rendezvousId=${rendezvousId}" />
</jstl:if>
<spring:message code="question.question" var="question" />

<display:table name="questions" id="row" requestURI="${uri}" pagesize="5">
	<display:column title="${question}" sortable="true">
		<jstl:out value="${row.text}" />
	</display:column>
	<display:column>
			<spring:message code="question.delete" var="deleteQuestion" />	
			<a href="question/create.do?rendezvousId=${rendezvousId}">Ver answers</a>
		</display:column>
	<jstl:if test="${creator == true}">
		<display:column>
			<spring:message code="question.delete" var="deleteQuestion" />	
			<a href="question/delete.do?questionId=${row.id}"><jstl:out value="${deleteQuestion}"/></a>
		</display:column>
		
		<display:column>
			<spring:message code="question.edit" var="editQuestion" />	
			<a href="question/edit.do?questionId=${row.id}"><jstl:out value="${editQuestion}"/></a>
		</display:column>
	</jstl:if>
	
</display:table>

<jstl:if test="${creator == true}">
	<br/>
	<spring:message code="question.add" var="addQuestion" />	
	<a href="question/create.do?rendezvousId=${rendezvousId}"><jstl:out value="${addQuestion}"/></a>
</jstl:if>


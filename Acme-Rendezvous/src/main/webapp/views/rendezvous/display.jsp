<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:choose>

	<jstl:when test="${not empty rendezvous}">
		<security:authorize access="hasRole('USER')">
			<jstl:if test="${(user == rendezvous.creator) && draft==true}">
				<a href="rendezvous/user/edit.do?rendezvousId=${param.rendezvousId}"><spring:message
						code="rendezvous.display.editRendezvous" /></a>
				<br />
				<br />
			</jstl:if>
		</security:authorize>


		<!-- Name -->
		<spring:message code="rendezvous.display.name" />:
				<jstl:out value="${rendezvous.name}"></jstl:out>
		<br />
		<br />
		<!-- Description -->
		<spring:message code="rendezvous.display.description" />:
				<jstl:out value="${rendezvous.description}"></jstl:out>
		<br />
		<br />
	
		<jstl:if test="${rendezvous.deleted==false  && (rendezvous.draft == false)  && (rendezvous.moment > currentDate)}">
			<a href="question/list.do?rendezvousId=${rendezvous.id}"><spring:message
					code="rendezvous.display.list.question" /></a>
			<br />
		</jstl:if>
		


	</jstl:when>

	<jstl:otherwise>
		<!-- Comprobamos que sea user, si lo es pues le dejamos que pueda crear un rendezvous -->
		<security:authorize access="hasRole('USER')">
			<a href="rendezvous/user/create.do"><spring:message
					code="rendezvous.display.create" /></a>
			<br />
			<br />
		</security:authorize>
	</jstl:otherwise>
</jstl:choose>


<!-- Creamos el boton de atras, tenemos diferenetes URL si esta autenticado o no -->
<security:authorize access="hasRole('USER')">
	<jstl:choose>
		<jstl:when
			test="${(requestURI == 'rendezvous/user/list.do') && user == rendezvous.creator}">
			<spring:url var="urlListRendezvus" value="rendezvous/user/list.do" />
		</jstl:when>

		<jstl:otherwise>
			<spring:url var="urlListRendezvus" value="rendezvous/list.do" />
		</jstl:otherwise>
	</jstl:choose>
</security:authorize>

<security:authorize
	access="hasAnyRole('ADMINISTRATOR') or isAnonymous()">
	<spring:url var="urlListRendezvus" value="rendezvous/list.do" />
</security:authorize>

<input type="button" name="back"
	value="<spring:message code="rendezvous.display.back" />"
	onclick="javascript: relativeRedir('${urlListRendezvus}');" />
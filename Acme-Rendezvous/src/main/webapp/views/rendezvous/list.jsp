<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<%-- --%>

<display:table name="rendezvous" id="row" requestURI="${requestURI}"
	pagesize="5">

	<spring:message code="rendezvous.list.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" />

	<spring:message code="rendezvous.list.description"
		var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" />

	<spring:message code="rendezvous.list.users" var="usersHeader" />
	<display:column property="users" title="${usersHeader}" />

	<%-- 	<spring:message code="rendezvous.list.users" var="usersHeader" /> --%>
	<%-- 	<jstl:forEach var="i" items="${row.users}"> --%>
	<%-- 		<display:column title="${usersHeader}"> --%>
	<%-- 		 <jstl:out value="${i.name}"/> --%>
	<%-- <%-- 		 <jstl:if test="${empty i.name}"> --%>
	<%-- <%-- 		 <spring:message code="rendezvous.list.messageToEmptyUsers" /> --%>
	<%-- <%-- 		 </jstl:if> --%>
	<%-- 	</display:column> --%>
	<%-- 	</jstl:forEach> --%>

	<spring:message code="rendezvous.list.creator" var="creatorHeader" />
	<display:column property="creator" title="${creatorHeader}" />

	<spring:message code="rendezvous.list.links" var="linksHeader" />
	<display:column property="links" title="${linksHeader}" />

	<%--Dependiendo de la URL que tengamos mostramos --%>
	<security:authorize access="hasRole('USER')">
		<jstl:choose>
			<jstl:when
				test="${(requestURI == 'rendezvous/user/list.do') && user == row.creator}">
				<spring:url var="urlDisplayRendezvous"
					value="rendezvous/user/display.do?rendezvousId=${row.getId()}" />
			</jstl:when>

			<jstl:otherwise>
				<spring:url var="urlDisplayRendezvous"
					value="rendezvous/display.do?rendezvousId=${row.getId()}" />
			</jstl:otherwise>
		</jstl:choose>
	</security:authorize>

	<security:authorize access="isAnonymous()">
		<spring:url var="urlDisplayRendezvous"
			value="rendezvous/display.do?rendezvousId=${row.getId()}" />
	</security:authorize>

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false">
		<a href="<spring:url value="${urlDisplayRendezvous}" />"> <spring:message
				code="rendezvous.list.display" />
		</a>
	</display:column>

	<!-- EDIT, solo mostraremos el edit cuando estemos en los rendezvous del usuario logado -->
	<jstl:if
		test="${(requestURI == 'rendezvous/user/list.do') && (row.draft==true) && (user==row.creator) }">
		<security:authorize access="hasRole('USER')">
			<display:column title="" sortable="false">
				<a
					href="<spring:url value="rendezvous/user/edit.do?rendezvousId=${row.getId()}" />">
					<spring:message code="rendezvous.list.edit" />
				</a>
			</display:column>
		</security:authorize>
	</jstl:if>
</display:table>

<%--Si la url es la de un user, tendremos que mostrar los rendezvous del user no todos --%>
<jstl:if test="${requestURI == 'rendezvous/user/list.do'}">

	<security:authorize access="hasRole('USER')">
		<a href="rendezvous/user/create.do"><spring:message
				code="rendezvous.list.create" /></a>
	</security:authorize>
</jstl:if>




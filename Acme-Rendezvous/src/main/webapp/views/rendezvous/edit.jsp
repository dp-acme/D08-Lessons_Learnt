<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<security:authorize access="hasRole('USER')">
	<!-- Abrimos el formulario -->
	<form:form action="rendezvous/user/edit.do" modelAttribute="rendezvous">

		<!-- Indicamos los campos ocultos -->
		<form:hidden path="adultsOnly" />
		<form:hidden path="draft" />
		<form:hidden path="deleted" />
		<form:hidden path="questions" />
		<form:hidden path="announcements" />
		<form:hidden path="users" />
		<form:hidden path="creator" />
		<form:hidden path="links" />
		<form:hidden path="id" />
		<form:hidden path="version" />

		<form:hidden path="moment" />

		<!-- Indicamos los campos de los formularios -->
		<form:label path="name">
			<spring:message code="rendezvous.name" />: 
				</form:label>
		<form:input path="name" type="text" required="required" />
		<form:errors path="name" cssClass="error" />
		<br />
		<br />


		<form:label path="description">
			<spring:message code="rendezvous.description" />: 
				</form:label>
		<br />
		<form:textarea path="description" required="required" />
		<form:errors path="description" cssClass="error" />
		<br />
		<br />


		<form:label path="picture">
			<spring:message code="rendezvous.picture" />: 
				</form:label>
		<br />
		<form:textarea path="picture" />
		<form:errors path="picture" cssClass="error" />
		<br />
		<br />


		<form:label path="latitude">
			<spring:message code="rendezvous.latitude" />: 
				</form:label>
		<br />
		<form:textarea path="latitude" />
		<form:errors path="latitude" cssClass="error" />
		<br />
		<br />


		<form:label path="longitude">
			<spring:message code="rendezvous.longitude" />: 
				</form:label>
		<br />
		<form:textarea path="longitude" />
		<form:errors path="longitude" cssClass="error" />
		<br />
		<br />

		<!-- Botones del formulario -->
		<input type="submit" name="save"
			value="<spring:message code="rendezvous.save"/>" />
		<jstl:if test="${rendezvous.id != 0}">
			<input type="submit" name="delete"
				value="<spring:message code="rendezvous.delete"/>" />
		</jstl:if>
		<input type="button" name="cancel"
			value="<spring:message code="rendezvous.cancel" />"
			onclick="javascript: relativeRedir('rendezvous/user/list.do');" />
	</form:form>
</security:authorize>
package controllers;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.QuestionService;
import services.RendezvousService;
import domain.Answer;
import domain.Question;
import domain.Rendezvous;
import domain.User;

@Controller
@RequestMapping("/question")
public class QuestionController extends AbstractController{
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	@Autowired
	private ActorService actorService;

	public QuestionController() {
		super();
	}
	
	@RequestMapping(value = "/list", method=RequestMethod.GET)
	public ModelAndView list(@RequestParam("rendezvousId") int rendezvousId){
		ModelAndView result;
		Collection<Question> questions;
		Boolean creator = null;
		Rendezvous rendezvous;
		
		rendezvous = rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);

		questions = rendezvous.getQuestions();
		
		Assert.isTrue(!rendezvous.isDeleted());
	
		try{
			UserAccount principal = LoginService.getPrincipal();
			if(principal != null){
				User user = (User) actorService.findByUserAccountId(principal.getId());
				if(user.getMyRendezvouses().contains(rendezvous)){
					creator = true;
				}else{
					Assert.isTrue(rendezvous.getMoment().before(new Date()));
				}
			}
		}catch(Exception e){
			creator = false;
			Assert.isTrue(rendezvous.getMoment().before(new Date()));
		}
	
		result = new ModelAndView("question/list");
		result.addObject("questions", questions);
		result.addObject("creator", creator);
		result.addObject("rendezvousId", rendezvous.getId());
		
		return result;
	}
	
	@RequestMapping(value = "/create", method=RequestMethod.GET)
	public ModelAndView create(@RequestParam("rendezvousId") int rendezvousId){
		ModelAndView result;
		Rendezvous rendezvous;
		Question question;
		
		rendezvous = rendezvousService.findOne(rendezvousId);
		question = this.questionService.create(rendezvous.getId());

		result = new ModelAndView("question/edit");
		result.addObject("question", question);
		result.addObject("rendezvous", rendezvous);
		
		return result;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam int questionId) {
		ModelAndView result;
		Question question;

		question = questionService.findOne(questionId);

		Assert.notNull(question);

		result = createEditModelAndView(question);

		return result;
	}
	
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam int questionId) {
		ModelAndView result;

		Question question = questionService.findOne(questionId);
		
		try {
			questionService.delete(question);
			
			result = new ModelAndView("redirect:/question/list.do?rendezvousId="+question.getRendezvous().getId());
			
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(question, "question.commit.error");
		}

		return result;
	}
	
	@RequestMapping(value = "/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@Valid Question question, BindingResult binding){
		ModelAndView result;
		Rendezvous rendezvous;
		Collection<Question> questions;
		
		if(binding.hasErrors()){
			result = createEditModelAndView(question);
		}else{
			try{
				rendezvous=question.getRendezvous();
				questions = rendezvous.getQuestions();
				questions.add(question);
				rendezvous.setQuestions(questions);
				rendezvousService.save(rendezvous);
				questionService.save(question);
				result = new ModelAndView("redirect:list.do?rendezvousId="+rendezvous.getId());
			}catch(Throwable oops){
				result = createEditModelAndView(question, oops.getMessage());
			}
		}
		
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Question question){
		ModelAndView result;
		
		result = createEditModelAndView(question, null);
		
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Question  question, String messageCode){
		ModelAndView result;
		Rendezvous rendezvous;
		
		rendezvous = question.getRendezvous();
		
		result = new ModelAndView("question/edit");
		result.addObject("question", question);
		result.addObject("rendezvous", rendezvous);
		result.addObject("message", messageCode);
		
		return result;
 	}
}

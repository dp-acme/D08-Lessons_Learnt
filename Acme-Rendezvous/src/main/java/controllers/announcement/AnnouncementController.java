/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.announcement;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.AnnouncementService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Announcement;
import domain.Rendezvous;

@Controller
@RequestMapping("/announcement")
public class AnnouncementController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AnnouncementController() {
		super();
	}
	
	// Services ---------------------------------------------------------------		
	
	@Autowired
	private AnnouncementService announcementService;
	
	@Autowired
	private RendezvousService rendezvousService;

	// List ---------------------------------------------------------------		

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = false, value = "rendezvousId") Integer rendezvousId) {
		ModelAndView result;
		
		Collection<Announcement> announcements;
		
		if (rendezvousId != null) {
			announcements = announcementService.findFromRendezvous(rendezvousId);
		} else {
			UserAccount principal;
			
			principal = LoginService.getPrincipal();
			
			announcements = announcementService.findFromUserAccountOrderedByDate(principal.getId());
		}
		
		result = new ModelAndView("announcement/list");
		result.addObject("announcements", announcements);

		return result;
	}
	
	// Create ---------------------------------------------------------------		

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true, value = "rendezvousId") int rendezvousId) {
		Assert.isTrue(rendezvousId != 0);
		
		ModelAndView result;
		Announcement announcement;
		UserAccount principal;
		Rendezvous rendezvous;

		principal = LoginService.getPrincipal();
		rendezvous = rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);
		
		Assert.isTrue(rendezvous.getCreator().getUserAccount().equals(principal));

		announcement = announcementService.create(rendezvousId);

		result = createEditModelAndView(announcement);

		return result;
	}
	
	// Save ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Announcement announcement, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(announcement);
		else
			try {
				announcementService.save(announcement);
				result = new ModelAndView("redirect:list.do?rendezvousId?=" + announcement.getRendezvous().getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(announcement, "announcement.commit.error");
			}

		return result;
	}
	
	// Delete ---------------------------------------------------------------		

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam(required = true, value = "announcementId") int announcementId) {
		Assert.isTrue(announcementId != 0);
		
		ModelAndView result;
		Announcement announcement;
		Rendezvous rendezvous;
		
		announcement = announcementService.findOne(announcementId);
		Assert.notNull(announcement);
		
		rendezvous = announcement.getRendezvous();

		try {
			announcementService.delete(announcement);
			result = new ModelAndView("redirect:list.do?rendezvousId=" + rendezvous.getId());
		} catch (final Throwable oops) {
			result = new ModelAndView("redirect:list.do?rendezvousId=" + rendezvous.getId());
			result.addObject("message", "announcement.commit.error");
		}

		return result;
	}
	
	// Ancillary methods ---------------------------------------------------------------		

	protected ModelAndView createEditModelAndView(Announcement announcement) {
		ModelAndView result;

		result = createEditModelAndView(announcement, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Announcement announcement, String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("announcement/edit");
		result.addObject("announcement", announcement);

		result.addObject("message", messageCode);

		return result;
	}
	
}

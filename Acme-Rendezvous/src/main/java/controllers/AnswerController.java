package controllers;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.AnswerService;
import services.QuestionService;
import services.RendezvousService;
import domain.Answer;
import domain.Question;
import domain.Rendezvous;
import domain.User;

@Controller
@RequestMapping("/answer")
public class AnswerController extends AbstractController{

	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private AnswerService answerService;
	
	@RequestMapping(value = "/list", method=RequestMethod.GET)
	public ModelAndView list(@RequestParam("questionId") int questionId){
		ModelAndView result;
		Collection<Answer> answers;
		Question question;
		Rendezvous rendezvous;
		
		question = questionService.findOne(questionId);
		rendezvous = question.getRendezvous();
		
		Assert.isTrue(!rendezvous.isDeleted());
		Assert.notNull(question);		

		answers = question.getAnswers();
	
		result = new ModelAndView("answer/list");
		result.addObject("answers", answers);
		result.addObject("rendezousId", rendezvous.getId());
		
		return result;
	}
	
	@RequestMapping(value = "/user/edit", method=RequestMethod.GET)
	public ModelAndView create(@RequestParam("rendezvousId") int rendezvousId){
		ModelAndView result;
		Rendezvous rendezvous;
		Collection<Question> questions;
		User user;

		rendezvous = rendezvousService.findOne(rendezvousId);
		questions = rendezvous.getQuestions();
		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		Assert.isTrue(!rendezvous.isDeleted());
		Assert.isTrue(!rendezvous.isDraft());
		Assert.isTrue(rendezvous.getMoment().after(new Date()));
		Assert.isTrue(!rendezvous.getCreator().equals(user));
	
		if(questions.size()>0){
			result = new ModelAndView("answer/create");
			result.addObject("questions", questions);
			result.addObject("rendezvous", rendezvous);
		}else{
			rendezvousService.addUser(rendezvous, user);
			result = new ModelAndView("redirect:/rendezvous/display.do?rendezvousId="+rendezvous.getId());
		}
		return result;
	}
	

	@RequestMapping(value = "/user/edit", method=RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("answers") String answers, @RequestParam("rendezvousId") int rendezvousId, BindingResult binding){
		Rendezvous rendezvous;
		List<Question> questions;
		User user;
		ModelAndView result;

		String[] answer = answers.split("\n");
		rendezvous = rendezvousService.findOne(rendezvousId);
		questions = (List<Question>) rendezvous.getQuestions();
		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		try{
			Assert.isTrue(questions.size() == answer.length);
			Assert.isTrue(!user.getMyRSVPs().contains(rendezvous));
			for(int i = 0; i<questions.size(); i++){
				Question question = questions.get(i);
				Answer answerC = answerService.create(question.getId());
				answerC.setAnswer(answer[i]);
				answerC = answerService.save(answerC);
				questionService.addAnswer(answerC);
			}
			
			rendezvousService.addUser(rendezvous, user);
			result = new ModelAndView("redirect:/question/list.do?rendezvousId="+rendezvous.getId());
			
		}catch (Exception e) {
			result = new ModelAndView("answer/create");
			result.addObject("questions", questions);
			result.addObject("rendezvous", rendezvous);
			result.addObject("error", true);
		}
		
		return result;
	}
	
}

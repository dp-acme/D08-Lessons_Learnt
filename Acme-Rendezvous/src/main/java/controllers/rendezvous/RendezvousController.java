/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.rendezvous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.RendezvousService;
import controllers.AbstractController;
import domain.Rendezvous;

@Controller
@RequestMapping("/rendezvous")
public class RendezvousController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	RendezvousService	rendezvousService;


	// Constructors -----------------------------------------------------------

	public RendezvousController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/display")
	public ModelAndView display(@RequestParam(value = "rendezvousId", required = true) Integer rendezvousId) {
		ModelAndView result;
		Rendezvous rendezvous;
		
		//Fecha actual del sistema
		Date currentDate= new Date();
		
		result = new ModelAndView("rendezvous/display");

		rendezvous = rendezvousService.findOne(rendezvousId);

		result.addObject("rendezvous", rendezvous);
		result.addObject("currentDate", currentDate);
		
		return result;
	}

	//List -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		//Creamos el objeto a devolver
		ModelAndView result;
		//Creamos una coleccion de rendezvous para alamcenar todos
		Collection<Rendezvous> allRendezvous;
		//Cogemos todos los rendezvous del servicio
		allRendezvous = this.rendezvousService.findAll();

		//Creamos una lista para meter los rendezvous filtrados
		Collection<Rendezvous> rendezvous = new ArrayList<Rendezvous>();

		//Filtramos solo aquellos que esten publicados
		for (Rendezvous r : allRendezvous) {
			if (r.isDraft() == false && r.getMoment().after(new Date()) && r.isDeleted() == false) {
				rendezvous.add(r);
			}
		}

		result = new ModelAndView("rendezvous/list");
		//Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("rendezvous", rendezvous);
		result.addObject("requestURI", "rendezvous/list.do");

		return result;
	}
}

/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.rendezvous;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Rendezvous;
import domain.User;

@Controller
@RequestMapping("/rendezvous")
public class RendezvousUserController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	private RendezvousService	rendezvousService;

	@Autowired
	private ActorService		actorService;


	// Constructors -----------------------------------------------------------

	public RendezvousUserController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/user/display")
	public ModelAndView display(@RequestParam(value = "rendezvousId", required = true) Integer rendezvousId) {
		ModelAndView result;
		Rendezvous rendezvous;

		result = new ModelAndView("rendezvous/user/display");

		//Cogemos el usuario logeado
		User user;

		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		rendezvous = rendezvousService.findOne(rendezvousId);
		boolean draft = rendezvous.isDraft();
		
		result.addObject("rendezvous", rendezvous);
		result.addObject("user", user);
		result.addObject("requestURI", "rendezvous/user/list.do");
		result.addObject("draft", draft);

		return result;
	}

	//List -------------------------------------------------------------------------
	@RequestMapping("/user/list")
	public ModelAndView list() {
		//Creamos el objeto a devolver
		ModelAndView result;
		//Creamos una coleccion de rendezvous para alamcenar todos
		Collection<Rendezvous> rendezvous;

		//Cogemos el usuario logeado
		User user;

		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		//Cogemos los rendezvous del usuario logeado
		rendezvous = user.getMyRendezvouses();

		result = new ModelAndView("rendezvous/user/list");
		//Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("user", user);
		result.addObject("rendezvous", rendezvous);
		result.addObject("requestURI", "rendezvous/user/list.do");
		
		
		return result;
	}
	
	@RequestMapping("/user/create")
	public ModelAndView create() {
		ModelAndView result;
		Rendezvous rendezvous;
		User user;

		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(user);
		rendezvous = rendezvousService.create();

		result = createEditModelAndView(rendezvous);
		result.addObject("rendezvous", rendezvous);

		return result;
	}

	//Edit------------------------------------------------------------
	@RequestMapping("/user/edit")
	public ModelAndView edit(@RequestParam int rendezvousId) {
		ModelAndView result;
		Rendezvous rendezvous;
		User user;

		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(user);

		rendezvous = rendezvousService.findOne(rendezvousId);

		Assert.notNull(rendezvous);
		Assert.isTrue(rendezvous.getCreator().getUserAccount().equals(LoginService.getPrincipal()));

		result = createEditModelAndView(rendezvous);
		result.addObject("rendezvous", rendezvous);

		return result;
	}

	@RequestMapping(value = "/user/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Rendezvous rendezvous, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(rendezvous);
		} else {
			try {
				this.rendezvousService.save(rendezvous);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(rendezvous, "rendezvous.commit.error");
			}
		}

		return result;
	}

	private ModelAndView createEditModelAndView(Rendezvous rendezvous) {
		ModelAndView result;

		result = createEditModelAndView(rendezvous, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Rendezvous rendezvous, String messageCode) {
		ModelAndView result;
		User user;

		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		result = new ModelAndView("rendezvous/user/edit");

		result.addObject("rendezvous", rendezvous);
		result.addObject("message", messageCode);

		return result;
	}
}

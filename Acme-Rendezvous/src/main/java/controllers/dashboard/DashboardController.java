/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import controllers.AbstractController;

@Controller
@RequestMapping("/dashboard")
public class DashboardController extends AbstractController {

	// Services -----------------------------------------------------------
	
	@Autowired
	AdministratorService administratorService;
	
	// Constructors -----------------------------------------------------------

	public DashboardController() {
		super();
	}

	@RequestMapping("/display")
	public ModelAndView display() {
		String[]avgSdRendezvousPerUser, avgSdUsersPerRendezvous, 
			avgSdRSVPsPerUser, avgSdAnnoucementsPerRendezvous, 
			avgSdQuestionsPerRendezvous, avgSdRepliesPerComment, 
			avgSdAnswersPerRendezvous;
		
		String ratioCreatorsOverNonCreators;
		
		ModelAndView result;

		result = new ModelAndView("dashboard");
		
		avgSdRendezvousPerUser = administratorService.roundTo(administratorService.getAvgSdRendezvousPerUser(), 2);
		ratioCreatorsOverNonCreators = administratorService.roundToRatio(administratorService.ratioCreatorsOverNonCreators(), 2);
		avgSdUsersPerRendezvous = administratorService.roundTo(administratorService.getAvgSdUsersPerRendezvous(), 2);
		avgSdRSVPsPerUser = administratorService.roundTo(administratorService.getAvgSdRSVPsPerUser(), 2);
		avgSdAnnoucementsPerRendezvous = administratorService.roundTo(administratorService.getAvgSdAnnoucementsPerRendezvous(), 2);
		avgSdQuestionsPerRendezvous = administratorService.roundTo(administratorService.getAvgSdQuestionsPerRendezvous(), 2);
		
		avgSdRepliesPerComment = administratorService.roundTo(administratorService.getAvgSdRepliesPerComment(), 2);
		avgSdAnswersPerRendezvous = administratorService.roundTo(administratorService.getAvgSdAnswersPerRendezvous(), 2);
			
		result.addObject("avgSdRendezvousPerUser", avgSdRendezvousPerUser);
		result.addObject("ratioCreatorsOverNonCreators", ratioCreatorsOverNonCreators);
		result.addObject("avgSdUsersPerRendezvous", avgSdUsersPerRendezvous);
		result.addObject("avgSdRSVPsPerUser", avgSdRSVPsPerUser);
		result.addObject("avgSdAnnoucementsPerRendezvous", avgSdAnnoucementsPerRendezvous);
		result.addObject("avgSdQuestionsPerRendezvous", avgSdQuestionsPerRendezvous);
		
		result.addObject("avgSdRepliesPerComment", avgSdRepliesPerComment);
		result.addObject("avgSdAnswersPerRendezvous", avgSdAnswersPerRendezvous);
		
		result.addObject("10RendezvousesOrderedByUsers", administratorService.get10RendezvousesOrderedByUsers());
		result.addObject("rendezvousWithOver75percentAnnoucements", administratorService.getRendezvousWithOver75percentAnnoucements());
		result.addObject("rendezvousLinkedOver10percentRendezvouses", administratorService.getRendezvousLinkedOver10percentRendezvouses());
		
		return result;
	}

}
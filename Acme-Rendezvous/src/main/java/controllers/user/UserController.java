package controllers.user;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.UserService;
import domain.User;

@Controller
@RequestMapping("/user")
public class UserController {
	// Services ---------------------------------------------------------------
	@Autowired
	private UserService userService;
	
	// Constructors -----------------------------------------------------------

		public UserController() {
			super();
		}
		
		@RequestMapping(value="/create", method = RequestMethod.GET)
		public ModelAndView create(){
			ModelAndView result;
			User user;
			
			user = this.userService.create();
			result = this.createEditModelAndView(user);

			
			return result;
		}
		
		@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
		public ModelAndView save(@Valid User user, BindingResult binding){
			ModelAndView result;
			if(binding.hasErrors()){
				result = createEditModelAndView(user);
			} else { 
				try {
					userService.save(user);
					result = new ModelAndView("redirect:/");
				} catch (Throwable oops) {
					result = createEditModelAndView(user, "user.commit.error");
				}
			}
			return result;
		}
		
		@RequestMapping(value = "/list", method = RequestMethod.GET)
		public ModelAndView list() {
			ModelAndView result;
			Collection<User> users;

			users = userService.findAll();	
				
			result = new ModelAndView("user/list");
			result.addObject("users", users);
			result.addObject("requestURI", "user/list.do");

			return result;
		}
		
		@RequestMapping(value = "/display", method = RequestMethod.GET)
		public ModelAndView display(@RequestParam int userId) {
			ModelAndView result;
			User user;

			user = userService.findOne(userId);	
				
			result = new ModelAndView("user/display");
			result.addObject("user", user);
			result.addObject("requestURI", "user/display.do");

			return result;
		}
		
		
		
		protected ModelAndView createEditModelAndView(User user) {
			ModelAndView result; 
			
			result = createEditModelAndView(user, null);
			
			return result;
		}


		protected ModelAndView createEditModelAndView(User user,
				String messageCode) {
			ModelAndView result;
			result = new ModelAndView("user/create");
			result.addObject("user", user);
			result.addObject("message", messageCode);
			
			return result;
		}

}

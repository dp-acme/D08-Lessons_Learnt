
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import domain.Rendezvous;

public interface RendezvousRepository extends JpaRepository<Rendezvous, Integer> {

	//Obtener los Rendezvous que estan en finalMode (==true)
	@Query("select r from Rendezvous r where r.draft = true")
	Collection<Rendezvous> getRendezvousDraft();

	//Obtener los Rendezvous que no estan en finalMode (==fasle)
	@Query("select r from Rendezvous r where r.draft = false")
	Collection<Rendezvous> getRendezvousNoDraft();
}

package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.UserRepository;
import security.LoginService;
import security.UserAccount;
import domain.Rendezvous;
import domain.User;

@Service
@Transactional
public class UserService {
	// Managed repository ---------------------------------------------------
	@Autowired
	private UserRepository userRepository;
	
	// Supporting services ---------------------------------------------------
	
	// Constructor ---------------------------------------------------
	
	public UserService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	public User create() {
		
		User result;
		
		result = new User();
		result.setMyRendezvouses(new ArrayList<Rendezvous>());
		result.setMyRSVPs(new ArrayList<Rendezvous>());
		
		return result;
	}
	
	public User findOne(int userId) {
		Assert.isTrue(userId != 0);
		
		User result;
		
		result = userRepository.findOne(userId);
		
		return result;
	}
	
	public Collection<User> findAll() {		
		Collection<User> result;
		
		result = userRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public User save(User user) {
		Assert.notNull(user);
		User result;
		UserAccount userAccount;
		
		if(user.getId()!=0){
			userAccount = LoginService.getPrincipal();
			Assert.isTrue(user.getUserAccount().equals(userAccount));
		}else{
			String role;
			Md5PasswordEncoder encoder;

			if(SecurityContextHolder.getContext().getAuthentication()!=null
		&&SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray().length>0){
				role = SecurityContextHolder.getContext()
					.getAuthentication().getAuthorities().toArray()[0].toString();
			Assert.isTrue(role.equals("ROLE_ANONYMOUS"));
			}
			encoder = new Md5PasswordEncoder();
			user.getUserAccount().setPassword(encoder.encodePassword(user.getUserAccount().getPassword(),null));
		}
		
		result = userRepository.save(user);

		return result;
	}
	

	
	// Other business methods -------------------------------------------------

}

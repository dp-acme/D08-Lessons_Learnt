package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CommentRepository;
import security.Authority;
import domain.Actor;
import domain.Comment;
import domain.User;

@Service
@Transactional
public class CommentService {
	// Managed repository ---------------------------------------------------

	@Autowired
	private CommentRepository commentRepository;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------
	
	public CommentService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	public Comment create(User user) {
		Comment result;
		
		result = new Comment();
		result.setUser(user);
		result.setMoment(new Date(System.currentTimeMillis() - 1));
		
		return result;
	}
	
	public Comment findOne(int commentId) {
		Assert.isTrue(commentId != 0);
		
		Comment result;
		
		result = commentRepository.findOne(commentId);
		
		return result;
	}
	
	public Collection<Comment> findAll() {		
		Collection<Comment> result;
		
		result = commentRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Comment save(Comment comment) {
		Comment result;
		
		Actor principal;
		Authority auth;
		
		if(comment.getId() != 0){ //Comprobar credenciales
			principal = actorService.findPrincipal();
			
			auth = new Authority();
			auth.setAuthority(Authority.USER);
			
			Assert.isTrue(principal.getUserAccount().getAuthorities().contains(auth)); //Comprobar que verdaderamente es un User	
			Assert.isTrue(principal instanceof User); //Comprobar que el principal es un User (seguridad de c�digo)
			Assert.isTrue(comment.getUser().equals((User)principal)); //Comprobar que el comentario es suyo
		}
		
		result = commentRepository.save(comment);

		return result;
	}
	
	// Other business methods -------------------------------------------------
	
	public Comment flagAsDeleted(Comment comment){
		Assert.isTrue(!comment.getDeleted()); //Comprobar que no se trate de un comentario ya eliminado
		
		Comment result;
		
		Actor principal;
		Authority auth;
		
		principal = actorService.findPrincipal();
		
		auth = new Authority();
		auth.setAuthority(Authority.USER);
			
		if(principal.getUserAccount().getAuthorities().contains(auth)){ //Si se trata de un User
			Assert.isTrue(principal instanceof User); //Comprobar que el principal es un User (seguridad de c�digo)
			Assert.isTrue(comment.getUser().equals((User)principal)); //Comprobar que el comentario es suyo
		}
		
		comment.setDeleted(true);
		
		result = commentRepository.save(comment);
		
		return result;
	}

}

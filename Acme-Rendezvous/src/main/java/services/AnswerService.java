package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AnswerRepository;
import security.LoginService;
import security.UserAccount;
import domain.Answer;
import domain.User;

@Service
@Transactional
public class AnswerService {
	
	@Autowired
	private AnswerRepository answerRepository;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private QuestionService questionService;
	
	public AnswerService(){
		super();
	}
	
	public Answer create(int questionId){
		Answer result;
		User user;
		
		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		result = new Answer();
		
		result.setUser(user);
		result.setQuestion(questionService.findOne(questionId));
		
		return result;
	}
	
	public Answer findOne(int answerId){
		Assert.isTrue(answerId != 0);
		
		Answer result;
		
		result = answerRepository.findOne(answerId);
		
		return result;
	}
	
	public Collection<Answer> findAll(){
		Collection<Answer> result;
		
		result = answerRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public void delete(Answer answer){
		Assert.notNull(answer);	
		
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(answer.getUser().equals(principal));
			
		questionService.deleteAsnwer(answer);
		
		answerRepository.delete(answer);
	}
	
	public Answer save(Answer answer){
		Assert.notNull(answer);
		
		Answer result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(answer.getUser().getUserAccount().equals(principal));
		
		result = answerRepository.save(answer);
		
		if(answer.getId() == 0){
			questionService.addAnswer(result);
		}

		return result;
	}
	
	public void deleteAll(Collection<Answer> answers){
		answerRepository.deleteInBatch(answers);
	}
}

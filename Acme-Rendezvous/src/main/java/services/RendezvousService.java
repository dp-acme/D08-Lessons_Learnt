
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.RendezvousRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Administrator;
import domain.Announcement;
import domain.Answer;
import domain.Question;
import domain.Rendezvous;
import domain.User;

@Service
@Transactional
public class RendezvousService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private RendezvousRepository	rendezvousRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService			actorService;

	@Autowired
	private AnswerService			answerService;

	@Autowired
	private QuestionService			questionService;

	@Autowired
	private UserService				userService;


	// Constructor------------------------------------------------------------------------

	public RendezvousService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Rendezvous create() {

		//Comprobamos que el usuario logeado es un user
		this.checkIsUser();
		//Si es asi lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();

		User user;
		user = (User) this.actorService.findByUserAccountId(principal.getId());

		//Creamos el resultado
		Rendezvous result;
		result = new Rendezvous();

		//Creamos los atributos y relaciones
		//		Date moment;
		User creator;
		boolean draft;
		boolean deleted;
		boolean adultsOnly;

		Collection<Question> questions;
		Collection<Announcement> announcements;
		Collection<User> users;
		Collection<Rendezvous> links;

		//Inicializamos los atributos
		//moment = new Date();
		creator = user;
		draft = false; //Inicializamos a false para poder editarlo
		deleted = false;
		adultsOnly = false;

		questions = new ArrayList<Question>();
		announcements = new ArrayList<Announcement>();
		users = new ArrayList<User>();
		//A la lista de usuarios que van a asistir, introducimos el usuario 
		links = new ArrayList<Rendezvous>();

		//Asignamos atributos
		//result.setMoment(moment);
		result.setCreator(creator);
		result.setDraft(draft);
		result.setDeleted(deleted);
		result.setAdultsOnly(adultsOnly);

		result.setQuestions(questions);
		result.setAnnouncements(announcements);
		result.setUsers(users);
		result.setLinks(links);

		//Devolvemos resultado
		return result;
	}

	public Rendezvous findOne(final int rendezvousId) {
		//Comprobamos que el objeto que le pasamos no es nulo
		Assert.isTrue(rendezvousId != 0);
		//Creamos el objeto a devolver
		Rendezvous result;
		//Cogemos del repositorio el rendezvous que le pasamos como parametro
		result = this.rendezvousRepository.findOne(rendezvousId);
		//Devolvemos el resultado
		return result;

	}

	public Collection<Rendezvous> findAll() {
		//Creamos el objeto a devolver
		Collection<Rendezvous> result;
		//Cogemos del repositorio los rendezvous
		result = this.rendezvousRepository.findAll();
		//Devolvemos el resultado
		return result;
	}

	public Rendezvous save(final Rendezvous rendezvous) {
		//Comprobamos que el rendezvous que le pasamos no es nulo
		Assert.notNull(rendezvous);

		//Comprobamos que el usuario logeado es un user
		this.checkIsUser();

		//Comprobamos si esta en modo borrador(Draft)
		Assert.isTrue(rendezvous.isDraft());

		//Comprobamos si el atributo deleted no esta marcado como borrado
		Assert.isTrue(!rendezvous.isDeleted());

		//Si es un user lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();
		User user;

		user = (User) this.actorService.findByUserAccountId(principal.getId());

		//Comprobamos que el user del rendezvous es el mismo que el que esta logeado
		Assert.isTrue(rendezvous.getCreator().equals(user));

		//Creamos el objeto a devolver
		Rendezvous result;

		//Actualizamos el rendezvous
		result = this.rendezvousRepository.save(rendezvous);

		//Si es la primera vez que entra, a�adimos a la lista de rendezvous el rendezvous que queremos guardar ya que no puede ser nulo y tiene que tenerlo
		if (rendezvous.getId() == 0) {
			user.getMyRendezvouses().add(result);
			this.userService.save(user);
		}

		//Devolvemos rel resultado
		return result;
	}

	//Importante acutalizar despues el set deleted por qu tiene que cambiar
	//Tambien es importante si eres admin que si lo eres puedes eliminar un rendezvous

	public void delete(final Rendezvous rendezvous) {

		//Comprobamos que el objeto que le pasamos no es nulo
		Assert.notNull(rendezvous);

		//Comprobamos si esta en modo draft
		Assert.isTrue(rendezvous.isDraft());

		//Si esta marcado como borrado (Deleted)
		Assert.isTrue(!rendezvous.isDeleted());

		//Si es un user lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();
		Actor actor = this.actorService.findByUserAccountId(principal.getId());
		User user = (User) actor;

		if (actor instanceof User) {
			this.checkIsUser();
			Assert.isTrue(rendezvous.getCreator().equals(user));
		} else
			this.checkIsAdmin();

		//Comprobamos que el user del rendezvous es el mismo que el que esta logeado

		//Actualizamos referencias, borrando las relaciones que tiene Rendezvous

		Collection<Question> questions;

		//Inicializamos las relaciones
		questions = rendezvous.getQuestions();

		rendezvous.setUsers(new ArrayList<User>());
		//Recorro las questions y para cada question le borro las answers
		Collection<Answer> answers;
		for (Question q : questions) {
			//Cogemos todas las repuestas de cada pregunta
			answers = q.getAnswers();

			//Eliminamos las respuestas de la pregunta
			q.getAnswers().clear();
			questionService.save(q);

			answerService.deleteAll(answers);
		}

		//Recorrremos los asistentes al rendezvous y para cada uno eliminamos el RSVP
		for (User u : rendezvous.getUsers()) {
			u.getMyRSVPs().remove(rendezvous);
			userService.save(u);

		}

		//Actualizamos el atributo deleted a true
		rendezvous.setDeleted(true);

		rendezvousRepository.save(rendezvous);

	}
	// Other business methods ---------------------------------------------------

	private void checkIsUser() {
		//Comprueba que el usuario logeado es un user
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());

		Assert.isInstanceOf(User.class, actor);
	}

	private void checkIsAdmin() {
		//Comprueba que el usuario logeado es un user
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());

		Assert.isInstanceOf(Administrator.class, actor);
	}
}
